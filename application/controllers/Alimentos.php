<?php
      class Alimentos extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("alimento");
        }

        public function index(){
          $data["listado"]=$this->alimento->consultarTodos();
          $this->load->view("header");
          $this->load->view("alimentos/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("alimentos/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_ali){
          $data["alimento"]=$this->alimento->consultarPorId($id_ali);
          $this->load->view("header");
          $this->load->view("alimentos/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
            $id_ali=$this->input->post("id_ali");
            $datosAlimentoEditado=array(
                "codigo_ali"=>$this->input->post("codigo_ali"),
                "marca_ali"=>$this->input->post("marca_ali"),
                "tipo_ali"=>$this->input->post("tipo_ali"),
                "ingredientes_ali"=>$this->input->post("ingredientes_ali"),
                "felaboracion_ali"=>$this->input->post("felaboracion_ali"),
                "fcaducidad_ali"=>$this->input->post("fcaducidad_ali"),
                "precio_ali"=>$this->input->post("precio_ali"),
                "tallaperro_ali"=>$this->input->post("tallaperro_ali"),
                "pesoperro_ali"=>$this->input->post("pesoperro_ali"),
                "cantidad_ali"=>$this->input->post("cantidad_ali")
            );

            if($this->alimento->insertar($datosAlimentoEditado)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","ALIMENTO insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("alimentos/index");
        }

                public function guardarAlimento(){
                    $id_ali=$this->input->post("id_ali");
                    $datosAlimentoEditado=array(
                        "codigo_ali"=>$this->input->post("codigo_ali"),
                        "marca_ali"=>$this->input->post("marca_ali"),
                        "tipo_ali"=>$this->input->post("tipo_ali"),
                        "ingredientes_ali"=>$this->input->post("ingredientes_ali"),
                        "felaboracion_ali"=>$this->input->post("felaboracion_ali"),
                        "fcaducidad_ali"=>$this->input->post("fcaducidad_ali"),
                        "precio_ali"=>$this->input->post("precio_ali"),
                        "tallaperro_ali"=>$this->input->post("tallaperro_ali"),
                        "pesoperro_ali"=>$this->input->post("pesoperro_ali"),
                        "cantidad_ali"=>$this->input->post("cantidad_ali")
                    );

                    if($this->alimento->insertar($datosAlimentoEditado)){
                      // echo "INSERCION EXITOSA";
                        $this->session->set_flashdata("confirmacion","ALIMENTO insertado exitosamente.");
                        }
                          else {
                          $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                            }
                    redirect("alimentos/index");
                }

        public function procesarEliminacion($id_ali){
                  if($this->alimento->eliminar($id_ali)){
                    redirect("alimentos/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
