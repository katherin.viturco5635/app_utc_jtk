<?php
      class Citas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("cita");
        }

        public function index(){
          $data["listadoCitas"]=$this->cita->consultarTodos();
          $this->load->view("header");
          $this->load->view("citas/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("citas/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_cit){
          $data["listadoCitas"]=$this->cita->consultarTodos();
          $data["cita"]=$this->cita->consultarPorId($id_cit);
          $this->load-> view("header");
          $this->load-> view("citas/editar",$data);
          $this->load-> view("footer");
        }

        public function procesarActualizacion(){
          $id_cit=$this->input->post("id_cit");
          $datosCitaEditado=array(
            "nombresp_cit"=>$this->input->post("nombresp_cit"),
            "nombresd_cit"=>$this->input->post("nombresd_cit"),
            "historia_cit"=>$this->input->post("historia_cit"),
            "especialidad_cit"=>$this->input->post("especialidad_cit"),
            "fecha_cit"=>$this->input->post("fecha_cit"),
            "horario_cit"=>$this->input->post("horario_cit"),
            "nivelu_cit"=>$this->input->post("nivelu_cit"),
            "proposito_cit"=>$this->input->post("proposito_cit")
          );

        if ($this->cita->actualizar($id_cit,$datosCitaEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("citas/index");
        }
        else {
          echo "ERROR AL INSERTAR";
              }
            }


        public function guardarCita(){
            $datosNuevoCita=array(
                "nombresp_cit"=>$this->input->post("nombresp_cit"),
                "nombresd_cit"=>$this->input->post("nombresd_cit"),
                "historia_cit"=>$this->input->post("historia_cit"),
                "especialidad_cit"=>$this->input->post("especialidad_cit"),
                "fecha_cit"=>$this->input->post("fecha_cit"),
                "horario_cit"=>$this->input->post("horario_cit"),
                "nivelu_cit"=>$this->input->post("nivelu_cit"),
                "proposito_cit"=>$this->input->post("proposito_cit")
            );
            if($this->cita->insertar($datosNuevoCita)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","CITA insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("citas/index");
        }
        public function procesarEliminacion($id_cit){
                  if($this->cita->eliminar($id_cit)){
                    redirect("citas/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }



    }//cierre de la clase
?>
