<?php
      class Enfermedades extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("enfermedad");
        }

        public function index(){
          $data["listado"]=$this->enfermedad->consultarTodos();
          $this->load->view("header");
          $this->load->view("enfermedades/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("enfermedades/nuevo");
          $this->load->view("footer");
        }

        public function guardarEnfermedad(){
            $datosNuevoEnfermedad=array(
                "nombre_enf"=>$this->input->post("nombre_enf"),
                "descripcion_enf"=>$this->input->post("descripcion_enf"),
                "tratamiento_enf"=>$this->input->post("tratamiento_enf"),
            );
            if($this->enfermedad->insertar($datosNuevoEnfermedad)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","Enfermedad insertada exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("enfermedades/index");
        }
        public function procesarEliminacion($id_enf){
                  if($this->enfermedad->eliminar($id_enf)){
                    redirect("enfermedades/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
