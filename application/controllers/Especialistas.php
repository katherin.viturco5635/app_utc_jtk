<?php
      class Especialistas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("especialista");
        }

        public function index(){
          $data["listadoEspecialistas"]=$this->especialista->consultarTodos();
          $this->load->view("header");
          $this->load->view("especialistas/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("especialistas/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_esp){
          $data["listadoEspecialistas"]=$this->especialista->consultarTodos();
          $data["especialista"]=$this->especialista->consultarPorId($id_esp);
          $this->load-> view("header");
          $this->load-> view("especialistas/editar",$data);
          $this->load-> view("footer");
        }

        public function procesarActualizacion(){
          $id_esp=$this->input->post("id_esp");
          $datosEspecialistaEditado=array(
            "identificacion_esp"=>$this->input->post("identificacion_esp"),
            "nombres_esp"=>$this->input->post("nombres_esp"),
            "apellidos_esp"=>$this->input->post("apellidos_esp"),
            "fechan_esp"=>$this->input->post("fechan_esp"),
            "edad_esp"=>$this->input->post("edad_esp"),
            "direccion_esp"=>$this->input->post("direccion_esp"),
            "telefono_esp"=>$this->input->post("telefono_esp"),
            "tipos_esp"=>$this->input->post("tipos_esp"),
            "especialidad_esp"=>$this->input->post("especialidad_esp"),
            "nacionalidad_esp"=>$this->input->post("nacionalidad_esp"),
            "estado_esp"=>$this->input->post("estado_esp")
          );

        if ($this->especialista->actualizar($id_esp,$datosEspecialistaEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("especialistas/index");
        }
        else {
          echo "ERROR AL INSERTAR";
              }
            }

        public function guardarEspecialista(){
            $datosNuevoEspecialista=array(
                "identificacion_esp"=>$this->input->post("identificacion_esp"),
                "nombres_esp"=>$this->input->post("nombres_esp"),
                "apellidos_esp"=>$this->input->post("apellidos_esp"),
                "fechan_esp"=>$this->input->post("fechan_esp"),
                "edad_esp"=>$this->input->post("edad_esp"),
                "direccion_esp"=>$this->input->post("direccion_esp"),
                "telefono_esp"=>$this->input->post("telefono_esp"),
                "tipos_esp"=>$this->input->post("tipos_esp"),
                "especialidad_esp"=>$this->input->post("especialidad_esp"),
                "nacionalidad_esp"=>$this->input->post("nacionalidad_esp"),
                "estado_esp"=>$this->input->post("estado_esp")
            );
            if($this->especialista->insertar($datosNuevoEspecialista)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","ESPECIALISTAS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("especialistas/index");
        }
        public function procesarEliminacion($id_esp){
                  if($this->especialista->eliminar($id_esp)){
                    redirect("especialistas/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }



    }//cierre de la clase
?>
