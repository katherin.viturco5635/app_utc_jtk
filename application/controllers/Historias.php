
<?php
      class Historias extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("historia");
            $this->load->model("paciente");
        }

        public function index(){
          $data["listado"]=$this->historia->consultarTodos();
          $this->load->view("header");
          $this->load->view("historias/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data["listado"]=$this->historia->consultarTodos();
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $this->load->view("header");
          $this->load->view("historias/nuevo",$data);
          $this->load->view("footer");
        }
        public function editar($id_his){
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $data["listadoHistorias"]=$this->historia->consultarTodos();
          $data["historia"]=$this->historia->consultarPorId($id_his);
          $this->load-> view("header");
          $this->load-> view("historias/editar",$data);
          $this->load-> view("footer");
        }

        public function procesarActualizacion(){
          $id_his=$this->input->post("id_his");
          $datosHistoriaEditado=array(
            "fecha_his"=>$this->input->post("fecha_his"),
            // "nombres_cit"=>$this->input->post("nombres_cit"),
            "enfermedad_his"=>$this->input->post("enfermedad_his"),
            "tiempo_his"=>$this->input->post("tiempo_his"),
            "sintomas_his"=>$this->input->post("sintomas_his"),
            "peso_his"=>$this->input->post("peso_his"),
            "talla_his"=>$this->input->post("talla_his"),
            "otrae_his"=>$this->input->post("otrae_his"),
            "motivo_his"=>$this->input->post("motivo_his")
          );
          if ($this->historia->actualizar($id_his,$datosHistoriaEditado)) {
            // echo "INSERCION EXITOSA";
            redirect("historias/index");
          }
          else {
            echo "ERROR AL INSERTAR";
                }
              }

        public function guardarHistoria(){
            $datosNuevoHistoria=array(
                "fecha_his"=>$this->input->post("fecha_his"),
                "enfermedad_his"=>$this->input->post("enfermedad_his"),
                "tiempo_his"=>$this->input->post("tiempo_his"),
                "sintomas_his"=>$this->input->post("sintomas_his"),
                "peso_his"=>$this->input->post("peso_his"),
                "talla_his"=>$this->input->post("talla_his"),
                "otrae_his"=>$this->input->post("otrae_his"),
                "motivo_his"=>$this->input->post("motivo_his"),
                  "fk_id_pac"=>$this->input->post("fk_id_pac")
            );
            if($this->historia->insertar($datosNuevoHistoria)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","HISTORIAS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("historias/index");
        }
        public function procesarEliminacion($id_his){
                  if($this->historia->eliminar($id_his)){
                    redirect("historias/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }



    }//cierre de la clase
?>
