<?php
      class Medicamentos extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("medicamento");
        }

        public function index(){
          $data["listado"]=$this->medicamento->consultarTodos();
          $this->load->view("header");
          $this->load->view("medicamentos/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("medicamentos/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_med){
          $data["medicamento"]=$this->medicamento->consultarPorId($id_med);
          $this->load->view("header");
          $this->load->view("medicamentos/editar",$data);
          $this->load->view("footer");
        }
        public function procesarActualizacion(){
            $id_med=$this->input->post("id_med");
            $datosMedicamentoEditado=array(
                "codigo_med"=>$this->input->post("codigo_med"),
                "nombre_med"=>$this->input->post("nombre_med"),
                "lote_med"=>$this->input->post("lote_med"),
                "dosis_med"=>$this->input->post("dosis_med"),
                "felaboracion_med"=>$this->input->post("felaboracion_med"),
                "fvencimiento_med"=>$this->input->post("fvencimiento_med"),
                "cantidad_med"=>$this->input->post("cantidad_med")
            );
            if($this->medicamento->actualizar($id_med,$datosMedicamentoEditado)){
              redirect("medicamentos/index");
            }else{
                echo "ERROR AL INSERTAR";
            }
        }

        public function guardarMedicamento(){
            $datosNuevoMedicamento=array(
                "codigo_med"=>$this->input->post("codigo_med"),
                "nombre_med"=>$this->input->post("nombre_med"),
                "lote_med"=>$this->input->post("lote_med"),
                "dosis_med"=>$this->input->post("dosis_med"),
                "felaboracion_med"=>$this->input->post("felaboracion_med"),
                "fvencimiento_med"=>$this->input->post("fvencimiento_med"),
                "cantidad_med"=>$this->input->post("cantidad_med")
            );
            if($this->medicamento->insertar($datosNuevoMedicamento)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","MEDICAMENTOS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("medicamentos/index");
        }
        public function procesarEliminacion($id_med){
                  if($this->medicamento->eliminar($id_med)){
                    redirect("medicamentos/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
