<?php
      class Microchips extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("microchip");
        }

        public function index(){
          $data["listado"]=$this->microchip->consultarTodos();
          $this->load->view("header");
          $this->load->view("microchips/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("microchips/nuevo");
          $this->load->view("footer");
        }

        public function guardarmicrochip(){
            $datosNuevoMicrochip=array(
              "tipo_mic"=>$this->input->post("tipo_mic"),
              "codigo_mic"=>$this->input->post("codigo_mic"),
              "serial_mic"=>$this->input->post("serial_mic"),
              "color_mic"=>$this->input->post("color_mic"),
              "tamaño_mic"=>$this->input->post("tamaño_mic"),
              "fechaemision_mic"=>$this->input->post("fechaemision_mic")
            );
            if($this->microchip->insertar($datosNuevoMicrochip)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","MICROCHIP insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("microchips/index");
        }
        public function procesarEliminacion($id_mic){
                  if($this->microchip->eliminar($id_mic)){
                    redirect("microchips/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
