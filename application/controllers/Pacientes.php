<?php
      class Pacientes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("paciente");
        }

        public function index(){
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $this->load->view("header");
          $this->load->view("pacientes/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("pacientes/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_pac){
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $data["paciente"]=$this->paciente->consultarPorId($id_pac);
          $this->load-> view("header");
          $this->load-> view("pacientes/editar",$data);
          $this->load-> view("footer");
        }

        public function procesarActualizacion(){
          $id_pac=$this->input->post("id_pac");
          $datosPacienteEditado=array(
            "nombre_pac"=>$this->input->post("nombre_pac"),
            "sexo_pac"=>$this->input->post("sexo_pac"),
            "edad_pac"=>$this->input->post("edad_pac"),
            "esterilizado_pac"=>$this->input->post("esterilizado_pac"),
            "raza_pac"=>$this->input->post("raza_pac"),
            "color_pac"=>$this->input->post("color_pac")
          );

        if ($this->paciente->actualizar($id_pac,$datosPacienteEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("pacientes/index");
        }
        else {
          echo "ERROR AL INSERTAR";
              }
            }

        public function guardarPaciente(){
            $datosNuevoPaciente=array(
                "nombre_pac"=>$this->input->post("nombre_pac"),
                "sexo_pac"=>$this->input->post("sexo_pac"),
                "edad_pac"=>$this->input->post("edad_pac"),
                "esterilizado_pac"=>$this->input->post("esterilizado_pac"),
                "raza_pac"=>$this->input->post("raza_pac"),
                "color_pac"=>$this->input->post("color_pac")
            );
            if($this->paciente->insertar($datosNuevoPaciente)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","PACIENTES insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("pacientes/index");
        }

        public function procesarEliminacion($id_pac){
                  if($this->paciente->eliminar($id_pac)){
                    redirect("pacientes/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }

    }//cierre de la clase
?>
