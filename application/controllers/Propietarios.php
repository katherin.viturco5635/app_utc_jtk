<?php
      class Propietarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("propietario");
        }

        public function index(){
          $data["listado"]=$this->propietario->consultarTodos();
          $this->load->view("header");
          $this->load->view("propietarios/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("propietarios/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_pro){
          $data["propietario"]=$this->propietario->consultarPorId($id_pro);
          $this->load->view("header");
          $this->load->view("propietarios/editar",$data);
          $this->load->view("footer");
        }
        public function procesarActualizacion(){
            $id_pro=$this->input->post("id_pro");
            $datosPropietarioEditado=array(
                "cedula_pro"=>$this->input->post("cedula_pro"),
                "nombre_pro"=>$this->input->post("nombre_pro"),
                "apellido_pro"=>$this->input->post("apellido_pro"),
                "ciudad_pro"=>$this->input->post("ciudad_pro"),
                "direccion_pro"=>$this->input->post("direccion_pro"),
                "email_pro"=>$this->input->post("email_pro"),
                "telefono_pro"=>$this->input->post("telefono_pro")
            );
            if($this->propietario->actualizar($id_pro,$datosPropietarioEditado)){
              redirect("propietarios/index");
            }else{
                echo "ERROR AL INSERTAR";
            }
        }


        public function guardarPropietario(){
            $datosNuevoPropietario=array(
                "cedula_pro"=>$this->input->post("cedula_pro"),
                "nombre_pro"=>$this->input->post("nombre_pro"),
                "apellido_pro"=>$this->input->post("apellido_pro"),
                "ciudad_pro"=>$this->input->post("ciudad_pro"),
                "direccion_pro"=>$this->input->post("direccion_pro"),
                "email_pro"=>$this->input->post("email_pro"),
                "telefono_pro"=>$this->input->post("telefono_pro")
            );
            if($this->propietario->insertar($datosNuevoPropietario)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","PROPIETARIOS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("propietarios/index");
        }
        public function procesarEliminacion($id_pro){
                  if($this->propietario->eliminar($id_pro)){
                    redirect("propietarios/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la edula_pro
?>
