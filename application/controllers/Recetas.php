<?php
      class Recetas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("receta");
        }

        public function index(){
          $data["listado"]=$this->receta->consultarTodos();
          $this->load->view("header");
          $this->load->view("recetas/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("recetas/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_rec){
          $data["receta"]=$this->receta->consultarPorId($id_rec);
          $this->load->view("header");
          $this->load->view("recetas/editar",$data);
          $this->load->view("footer");
        }
        public function procesarActualizacion(){
            $id_rec=$this->input->post("id_rec");
            $datosRecetaEditado=array(
                "propietario_rec"=>$this->input->post("propietario_rec"),
                "paciente_rec"=>$this->input->post("paciente_rec"),
                "sexo_rec"=>$this->input->post("sexo_rec"),
                "raza_rec"=>$this->input->post("raza_rec"),
                "direccion_rec"=>$this->input->post("direccion_rec"),
                "edad_rec"=>$this->input->post("edad_rec"),
                "fecha_rec"=>$this->input->post("fecha_rec"),
                "descripcion_rec"=>$this->input->post("descripcion_rec")
            );
            if($this->receta->actualizar($id_rec,$datosRecetaEditado)){
              redirect("recetas/index");
            }else{
                echo "ERROR AL INSERTAR";
            }
        }

        public function guardarReceta(){
            $datosNuevoReceta=array(
                "propietario_rec"=>$this->input->post("propietario_rec"),
                "paciente_rec"=>$this->input->post("paciente_rec"),
                "sexo_rec"=>$this->input->post("sexo_rec"),
                "raza_rec"=>$this->input->post("raza_rec"),
                "direccion_rec"=>$this->input->post("direccion_rec"),
                "edad_rec"=>$this->input->post("edad_rec"),
                "fecha_rec"=>$this->input->post("fecha_rec"),
                "descripcion_rec"=>$this->input->post("descripcion_rec")
            );
            if($this->receta->insertar($datosNuevoReceta)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","RECETAS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("recetas/index");
        }
        public function procesarEliminacion($id_rec){
                  if($this->receta->eliminar($id_rec)){
                    redirect("recetas/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la edula_pro
?>
