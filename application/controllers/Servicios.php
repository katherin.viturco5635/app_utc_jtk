<?php
      class Servicios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("servicio");
        }

        public function index(){
          $data["listado"]=$this->servicio->consultarTodos();
          $this->load->view("header");
          $this->load->view("servicios/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("servicios/nuevo");
          $this->load->view("footer");
        }

        public function guardarservicio(){
            $datosNuevoServicio=array(
              "nombre_ser"=>$this->input->post("nombre_ser"),
              "descripcion_ser"=>$this->input->post("descripcion_ser"),
              "tipo_ser"=>$this->input->post("tipo_ser"),
              "costo_ser"=>$this->input->post("costo_ser"),
              "fecha1_ser"=>$this->input->post("fecha1_ser"),
              "fecha2_ser"=>$this->input->post("fecha2_ser")
            );
            if($this->servicio->insertar($datosNuevoServicio)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","SERVICIO insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("servicios/index");
        }
        public function procesarEliminacion($id_ser){
                  if($this->servicio->eliminar($id_ser)){
                    redirect("servicios/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
