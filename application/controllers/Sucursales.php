<?php
      class Sucursales extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("sucursal");
        }

        public function index(){
          $data["listado"]=$this->sucursal->consultarTodos();
          $this->load->view("header");
          $this->load->view("sucursales/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("sucursales/nuevo");
          $this->load->view("footer");
        }

        public function guardarsucursal(){
            $datosNuevoSucursal=array(
              "nombre_suc"=>$this->input->post("nombre_suc"),
              "direccion_suc"=>$this->input->post("direccion_suc"),
              "email_suc"=>$this->input->post("email_suc"),
              "telefono_suc"=>$this->input->post("telefono_suc")

            );
            if($this->sucursal->insertar($datosNuevoSucursal)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","SUCURSAL insertada exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("sucursales/index");
        }
        public function procesarEliminacion($id_suc){
                  if($this->sucursal->eliminar($id_suc)){
                    redirect("sucursales/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
