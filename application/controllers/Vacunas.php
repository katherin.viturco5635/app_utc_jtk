<?php
      class Vacunas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("vacuna");
        }

        public function index(){
          $data["listado"]=$this->vacuna->consultarTodos();
          $this->load->view("header");
          $this->load->view("vacunas/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("vacunas/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_vac){
          $data["vacuna"]=$this->vacuna->consultarPorId($id_vac);
          $this->load->view("header");
          $this->load->view("vacunas/editar",$data);
          $this->load->view("footer");
        }
        public function procesarActualizacion(){
          $id_vac=$this->input->post("id_vac");
          $datosVacunaEditado=array(
              "codigo_vac"=>$this->input->post("codigo_vac"),
              "nombre_vac"=>$this->input->post("nombre_vac"),
              "felaboracion_vac"=>$this->input->post("felaboracion_vac"),
              "fvencimiento_vac"=>$this->input->post("fvencimiento_vac"),
              "tventas_vac"=>$this->input->post("tventas_vac"),
              "precio_vac"=>$this->input->post("precio_vac"),
              "indicaciones_vac"=>$this->input->post("indicaciones_vac"),
              "cantidad_vac"=>$this->input->post("cantidad_vac")
          );
          if($this->vacuna->actualizar($id_vac,$datosVacunaEditado)){
            redirect("vacunas/index");
          }else{
              echo "ERROR AL INSERTAR";
          }
        }


        public function guardarVacuna(){
            $datosNuevoVacuna=array(
                "codigo_vac"=>$this->input->post("codigo_vac"),
                "nombre_vac"=>$this->input->post("nombre_vac"),
                "felaboracion_vac"=>$this->input->post("felaboracion_vac"),
                "fvencimiento_vac"=>$this->input->post("fvencimiento_vac"),
                "tventas_vac"=>$this->input->post("tventas_vac"),
                "precio_vac"=>$this->input->post("precio_vac"),
                "indicaciones_vac"=>$this->input->post("indicaciones_vac"),
                "cantidad_vac"=>$this->input->post("cantidad_vac")
            );
            if($this->vacuna->insertar($datosNuevoVacuna)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","VACUNAS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }redirect("vacunas/index");
        }
        public function procesarEliminacion($id_vac){
                  if($this->vacuna->eliminar($id_vac)){
                    redirect("vacunas/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la edula_pro
?>
