<?php
      class Visitas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("visita");
        }

        public function index(){
          $data["listadoVisitas"]=$this->visita->consultarTodos();
          $this->load->view("header");
          $this->load->view("visitas/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("visitas/nuevo");
          $this->load->view("footer");
        }

        public function editar($id_vis){
          $data["listadoVisitas"]=$this->visita->consultarTodos();
          $data["visita"]=$this->visita->consultarPorId($id_vis);
          $this->load-> view("header");
          $this->load-> view("visitas/editar",$data);
          $this->load-> view("footer");
        }

        public function procesarActualizacion(){
          $id_vis=$this->input->post("id_vis");
          $datosVisitaEditado=array(
            "nombrep_vis"=>$this->input->post("nombrep_vis"),
            "nombred_vis"=>$this->input->post("nombred_vis"),
            "telefono_vis"=>$this->input->post("telefono_vis"),
            "historia_vis"=>$this->input->post("historia_vis"),
            "direccion_vis"=>$this->input->post("direccion_vis"),
            "sintomas_vis"=>$this->input->post("sintomas_vis"),
            "dolor_vis"=>$this->input->post("dolor_vis")
          );

        if ($this->visita->actualizar($id_vis,$datosVisitaEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("visitas/index");
        }
        else {
          echo "ERROR AL INSERTAR";
              }
            }


        public function guardarVisita(){
            $datosNuevoVisita=array(
                "nombrep_vis"=>$this->input->post("nombrep_vis"),
                "nombred_vis"=>$this->input->post("nombred_vis"),
                "telefono_vis"=>$this->input->post("telefono_vis"),
                "historia_vis"=>$this->input->post("historia_vis"),
                "direccion_vis"=>$this->input->post("direccion_vis"),
                "sintomas_vis"=>$this->input->post("sintomas_vis"),
                "dolor_vis"=>$this->input->post("dolor_vis")
            );
            if($this->visita->insertar($datosNuevoVisita)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","VISITAS insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("visitas/index");
        }
        public function procesarEliminacion($id_vis){
                  if($this->visita->eliminar($id_vis)){
                    redirect("visitas/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }



    }//cierre de la clase
?>
