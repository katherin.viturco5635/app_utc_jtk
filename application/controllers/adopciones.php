<?php
      class Adopciones extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("adopcion");
        }

        public function index(){
          $data["listado"]=$this->adopcion->consultarTodos();
          $this->load->view("header");
          $this->load->view("adopciones/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("adopciones/nuevo");
          $this->load->view("footer");
        }


        public function guardarAdopcion(){
            $datosNuevoAdopcion=array(
              "nombremascota_ado"=>$this->input->post("nombremascota_ado"),
              "nombredueño_ado"=>$this->input->post("nombredueño_ado"),
              "descripcion_ado"=>$this->input->post("descripcion_ado"),
              "tipomascota_ado"=>$this->input->post("tipomascota_ado"),
              "costo_ado"=>$this->input->post("costo_ado"),
              "fecha_ado"=>$this->input->post("fecha_ado")
            );
            if($this->adopcion->insertar($datosNuevoAdopcion)){
              // echo "INSERCION EXITOSA";
                $this->session->set_flashdata("confirmacion","ADOPCION insertado exitosamente.");
                }
                  else {
                  $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                    }
            redirect("adopciones/index");
        }
        public function procesarEliminacion($id_ado){
                  if($this->adopcion->eliminar($id_ado)){
                    redirect("adopciones/index");
                  }else
                  echo "ERROR AL ELIMINAR";
                  }
    }//cierre de la clase
?>
