<?php
    class Adopcion extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("adopcion",$datos);
      }


      //funcion consultar todos
      public function consultarTodos(){
          $listadoAdopciones=$this->db->get("adopcion");
          if($listadoAdopciones->num_rows()>0){
            return $listadoAdopciones;//cuando SI hay alimentos
          }else{
            return false;//cuando NO hay alimentos
          }
      }
      
      public function eliminar($id_ado){
        $this->db->where("id_ado",$id_ado);
        return $this->db->delete("adopcion");
        }


   }//cierre de la clase

   //
 ?>
