<?php
    class Alimento extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("alimento",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_ali,$datos){
          $this->db->where("id_ali",$id_ali);
          return $this->db->update("alimento",$datos);
      }
      //funcion para sacar los datos anteriores almacenados
      public function consultarPorId($id_ali){
          $this->db->where("id_ali",$id_ali);
          $alimento=$this->db->get("alimento");
          if($alimento->num_rows()>0){
            return $alimento->row();
          }else{
            return false;
          }
      }


      //funcion para consultar todos los alimentos
      public function consultarTodos(){
          $listadoAlimentos=$this->db->get("alimento");
          if($listadoAlimentos->num_rows()>0){
            return $listadoAlimentos;//cuando SI hay alimentos
          }else{
            return false;//cuando NO hay alimentos
          }
      }

      public function eliminar($id_ali){
        $this->db->where("id_ali",$id_ali);
        return $this->db->delete("alimento");
        }


   }//cierre de la clase

   //
 ?>
