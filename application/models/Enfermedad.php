<?php
    class Enfermedad extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("enfermedad",$datos);
      }
      //funcion para consultar todos los alimentos
      public function consultarTodos(){
          $listadoEnfermedades=$this->db->get("enfermedad");
          if($listadoEnfermedades->num_rows()>0){
            return $listadoEnfermedades;//cuando SI hay alimentos
          }else{
            return false;//cuando NO hay alimentos
          }
      }

      public function eliminar($id_enf){
        $this->db->where("id_enf",$id_enf);
        return $this->db->delete("enfermedad");
        }


   }//cierre de la clase

   //
 ?>
