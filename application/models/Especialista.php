<?php
    class Especialista extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("especialista",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_esp,$datos){
        $this->db->where("id_esp",$id_esp);
      return $this->db->update("especialista",$datos);
    }
    //funcion para sacar el detalle de un clientes
      public function consultarPorId($id_esp){
      $this->db->where("id_esp",$id_esp);
        $especialista=$this->db->get("especialista");
        if($especialista->num_rows()>0){
          return $especialista->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }

      //funcion para consultar todos lo clientes
      public function consultarTodos(){
          $listadoEspecialistas=$this->db->get("especialista");
          if($listadoEspecialistas->num_rows()>0){
            return $listadoEspecialistas;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_esp){
              $this->db->where("id_esp",$id_esp);
              return $this->db->delete("especialista");
              }


   }//cierre de la clase



   //
 ?>
