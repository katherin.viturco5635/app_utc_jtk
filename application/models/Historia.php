<?php
    class Historia extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("historia",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_his,$datos){
        $this->db->where("id_his",$id_his);
      return $this->db->update("historia",$datos);
    }

    //funcion para consultar todos lo clientes
    public function consultarTodos(){
      //EDITAR
       $this->db->order_by("id_his","asc");
      $this->db->join("paciente","paciente.id_pac=historia.fk_id_pac");
        $listadoHistorias=$this->db->get("historia");
        if($listadoHistorias->num_rows()>0){
          return $listadoHistorias;//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
    }


      //funcion para sacar el detalle de un clientes
        public function consultarPorId($id_his){
        $this->db->where("id_his",$id_his);
        $this->db->join("paciente","paciente.id_pac=historia.fk_id_pac");
          $historia=$this->db->get("historia");
          if($historia->num_rows()>0){
            return $historia->row();//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
        }

      public function eliminar($id_his){
        $this->db->where("id_his",$id_his);
        return $this->db->delete("historia");
        }


   }//cierre de la clase



   //
 ?>
