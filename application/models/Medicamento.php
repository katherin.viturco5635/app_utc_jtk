<?php
    class Medicamento extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("medicamento",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_med,$datos){
          $this->db->where("id_med",$id_med);
          return $this->db->update("medicamento",$datos);
      }
      //funcion para sacar los datos anteriores almacenados
      public function consultarPorId($id_med){
          $this->db->where("id_med",$id_med);
          $medicamento=$this->db->get("medicamento");
          if($medicamento->num_rows()>0){
            return $medicamento->row();
          }else{
            return false;
          }
      }

      //funcion para consultar todos los medicamentos
      public function consultarTodos(){
          $listadoMedicamentos=$this->db->get("medicamento");
          if($listadoMedicamentos->num_rows()>0){
            return $listadoMedicamentos;//cuando SI hay medicamento
          }else{
            return false;//cuando NO hay medicamento
          }
      }

      public function eliminar($id_med){
        $this->db->where("id_med",$id_med);
        return $this->db->delete("medicamento");
        }


   }//cierre de la clase

   //
 ?>
