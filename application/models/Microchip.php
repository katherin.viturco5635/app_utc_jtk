<?php
    class Microchip extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("microchip",$datos);
      }
      //funcion para consultar todos los alimentos
      public function consultarTodos(){
          $listadoMicrochips=$this->db->get("microchip");
          if($listadoMicrochips->num_rows()>0){
            return $listadoMicrochips;//cuando SI hay alimentos
          }else{
            return false;//cuando NO hay alimentos
          }
      }

      public function eliminar($id_mic){
        $this->db->where("id_mic",$id_mic);
        return $this->db->delete("microchip");
        }


   }//cierre de la clase

   //
 ?>
