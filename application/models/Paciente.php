<?php
    class Paciente extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("paciente",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_pac,$datos){
        $this->db->where("id_pac",$id_pac);
      return $this->db->update("paciente",$datos);
    }
    //funcion para sacar el detalle de un clientes
      public function consultarPorId($id_pac){
      $this->db->where("id_pac",$id_pac);
        $paciente=$this->db->get("paciente");
        if($paciente->num_rows()>0){
          return $paciente->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }
      //funcion para consultar todos lo clientes
      public function consultarTodos(){
          $listadoPacientes=$this->db->get("paciente");
          if($listadoPacientes->num_rows()>0){
            return $listadoPacientes;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_pac){
              $this->db->where("id_pac",$id_pac);
              return $this->db->delete("paciente");
              }


   }//cierre de la clase



   //
 ?>
