<?php
    class Propietario extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("propietario",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_pro,$datos){
          $this->db->where("id_pro",$id_pro);
          return $this->db->update("propietario",$datos);
      }
      //funcion para sacar los datos anteriores almacenados
      public function consultarPorId($id_pro){
          $this->db->where("id_pro",$id_pro);
          $propietario=$this->db->get("propietario");
          if($propietario->num_rows()>0){
            return $propietario->row();
          }else{
            return false;
          }
      }

      //funcion para consultar todos los propietarios
      public function consultarTodos(){
          $listadoPropietarios=$this->db->get("propietario");
          if($listadoPropietarios->num_rows()>0){
            return $listadoPropietarios;//cuando SI hay propietarios
          }else{
            return false;//cuando NO hay propietario
          }
      }

      public function eliminar($id_pro){
        $this->db->where("id_pro",$id_pro);
        return $this->db->delete("propietario");
        }


   }//cierre de la clase

   //
 ?>
