<?php
    class Receta extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("receta",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_rec,$datos){
          $this->db->where("id_rec",$id_rec);
          return $this->db->update("receta",$datos);
      }
      //funcion para sacar los datos anteriores almacenados
      public function consultarPorId($id_rec){
          $this->db->where("id_rec",$id_rec);
          $receta=$this->db->get("receta");
          if($receta->num_rows()>0){
            return $receta->row();
          }else{
            return false;
          }
      }

      //funcion para consultar todos las recetass
      public function consultarTodos(){
          $listadoRecetas=$this->db->get("receta");
          if($listadoRecetas->num_rows()>0){
            return $listadoRecetas;//cuando SI hay receta
          }else{
            return false;//cuando NO hay receta
          }
      }

      public function eliminar($id_rec){
        $this->db->where("id_rec",$id_rec);
        return $this->db->delete("receta");
        }


   }//cierre de la clase

   //
 ?>
