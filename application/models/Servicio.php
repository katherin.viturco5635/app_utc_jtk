<?php
    class Servicio extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("servicio",$datos);
      }
      //funcion para consultar todos los alimentos
      public function consultarTodos(){
          $listadoServicios=$this->db->get("servicio");
          if($listadoServicios->num_rows()>0){
            return $listadoServicios;//cuando SI hay alimentos
          }else{
            return false;//cuando NO hay alimentos
          }
      }

      public function eliminar($id_ser){
        $this->db->where("id_ser",$id_ser);
        return $this->db->delete("servicio");
        }


   }//cierre de la clase

   //
 ?>
