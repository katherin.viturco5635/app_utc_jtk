<?php
    class Sucursal extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("sucursales",$datos);
      }
      //funcion para consultar todos los alimentos
      public function consultarTodos(){
          $listadoSucursales=$this->db->get("sucursales");
          if($listadoSucursales->num_rows()>0){
            return $listadoSucursales;//cuando SI hay alimentos
          }else{
            return false;//cuando NO hay alimentos
          }
      }

      public function eliminar($id_suc){
        $this->db->where("id_suc",$id_suc);
        return $this->db->delete("sucursales");
        }


   }//cierre de la clase

   //
 ?>
