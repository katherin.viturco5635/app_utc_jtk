<?php
    class Vacuna extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("vacuna",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_vac,$datos){
          $this->db->where("id_vac",$id_vac);
          return $this->db->update("vacuna",$datos);
      }
      //funcion para sacar los datos anteriores almacenados
      public function consultarPorId($id_vac){
          $this->db->where("id_vac",$id_vac);
          $vacuna=$this->db->get("vacuna");
          if($vacuna->num_rows()>0){
            return $vacuna->row();
          }else{
            return false;
          }
      }


      //funcion para consultar todos las vacunas
      public function consultarTodos(){
          $listadoVacunas=$this->db->get("vacuna");
          if($listadoVacunas->num_rows()>0){
            return $listadoVacunas;//cuando SI hay vacunas
          }else{
            return false;//cuando NO hay vacuna
          }
      }

      public function eliminar($id_vac){
        $this->db->where("id_vac",$id_vac);
        return $this->db->delete("vacuna");
        }


   }//cierre de la clase

   //
 ?>
