<?php
    class Visita extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("visita",$datos);
      }

      //funcion para actualizar
      public function actualizar($id_vis,$datos){
        $this->db->where("id_vis",$id_vis);
      return $this->db->update("visita",$datos);
    }
    //funcion para sacar el detalle de un clientes
      public function consultarPorId($id_vis){
      $this->db->where("id_vis",$id_vis);
        $visita=$this->db->get("visita");
        if($visita->num_rows()>0){
          return $visita->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }

      //funcion para consultar todos lo clientes
      public function consultarTodos(){
          $listadoVisitas=$this->db->get("visita");
          if($listadoVisitas->num_rows()>0){
            return $listadoVisitas;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_vis){
        $this->db->where("id_vis",$id_vis);
        return $this->db->delete("visita");
        }


   }//cierre de la clase



   //
 ?>
