<br>
<center>
  <hr>
  <h2>ADOPCIONES</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/adopciones/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i> Agregar una nueva adopcion
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DE LA MASCOTA</th>
        <th class="text-center">NOMBRE DEL DUEÑO</th>
        <th class="text-center">DESCRIPCION</th>
        <th class="text-center">TIPO DE ADOPCION</th>
        <th class="text-center">COSTO</th>
        <th class="text-center">FECHA DE ADOPCION</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
            <?php echo $filaTemporal->id_ado; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombremascota_ado; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombredueño_ado; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->descripcion_ado; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->tipomascota_ado; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->costo_ado; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->fecha_ado; ?>
          </td>
        <td class="text-center">
          <a href="btn btn-warning" class="btn btn-warning">  <i class="fa fa-pencil"></i>  </a>
          <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/adopciones/procesarEliminacion/<?php echo $filaTemporal->id_ado;?>" class="btn btn-danger"> <i class="fa fa-trash"></i> </a>

        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron adopciones</h3>

  </div>

<?php endif; ?>
