<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/adopciones/guardarAdopcion"
  method="post"
  >
  <hr>
  <center>
  <h1>DATOS DE LAS ADOPCIONES</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> NOMBRE DE LA MASCOTA </label>
    <input class="form-control" required="required"   type="text" name="nombremascota_ado" id="nombremascota_ado" placeholder="Registre el nombre. ejm:pepe.....">
    <br>
    <label for=""> NOMBRE DEL DUEÑO </label>
    <input
     class="form-control"     type="text" name="nombredueño_ado" id="nombredueño_ado" placeholder="Registre el nombre deldueño. ejm: LUIS.....">
     <br>
     <label for=""> DESCRIPCION </label>
     <input
      class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="descripcion_ado" id="descripcion_ado" placeholder="Ingrese una descripcion, ejm:lindo">
      <br>
     <label for="">TIPO DE MASCOTA: </label>
     <select class="form-control" name="tipomascota_ado" id="tipomascota_ado">
         <option value="">--Seleccione--</option>
         <option value="PERRO">PERRO</option>
         <option value="GATO">GATO</option>
         <option value="CABALLO">CABALLO</option>
         <option value="MASCOTA ramdom">OTROS</option>
     </select>
     <br>
    <label for=""> COSTO </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="number" name="costo_ado" id="costo_ado" placeholder="Registre el costso: Ejm, 6 ">
    <br>
    <label for="">FECHA DE ADOPCION </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fecha_ado" id="fecha_ado" placeholder="ejemplo: Seleccione">
    <br>

    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <button href="<?php echo base_url(); ?>/adopcion/index"  type="submit" name="button" class="btn btn-primary"> <i class="fa fa-cancel "></i> CANCELAR</button>
</form>
</div>
</div>
</div>
