<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/alimentos/procesarActualizacion"  method="post"  >
  <input type="hidden" name="id_ali" id="id_ali" value="<?php echo $alimento->id_ali; ?>">
  <hr>
  <center>
  <h1>ACTUALIZAR DATOS DE LOS ALIMENTOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CODIGO DEL ALIMENTO </label>
    <input class="form-control" value="<?php echo $alimento->codigo_ali; ?>"required="required"   type="text" name="codigo_ali" id="codigo_ali" placeholder="Registre el código. ejm: jgh-dfd5-h">
    <br>
    <label for=""> MARCA DEL ALIMENTO </label>
    <input class="form-control" value="<?php echo $alimento->marca_ali; ?>"required="required" onkeypress="return validar(event)"  type="text" name="marca_ali" id="marca_ali" placeholder="Ingrese la marca de alimento, ejm:PROCAN">
     <br>
     <label for="">TIPO DE ALIMENTO: </label>
     <select style="color:#000000" class="form-control" required="requiered" name="tipo_ali" id="tipo_ali">
         <option value="">--Seleccione--</option>
         <option value="Seco">Seco</option>
         <option value="Húmedo">Húmedo</option>
         <option value="Galletas">Galletas</option>
         <option value="Huesos">Huesos</option>
     </select>
     <br>
    <label for=""> INGREDIENTES: </label>
    <input class="form-control" value="<?php echo $alimento->ingredientes_ali; ?>" required="required" onkeypress="return validar(event)" type="text" name="ingredientes_ali" id="ingredientes_ali" placeholder="Registre: Ejm, pollo,zanahoria">
    <br>
    <label for="">FECHA DE ELABORACION </label>
    <input class="form-control" value="<?php echo $alimento->felaboracion_ali; ?>" required="required" onkeypress="return validar(event)" type="date" name="felaboracion_ali" id="felaboracion_ali" placeholder="ejemplo: Seleccione">
    <br>
    <label for="">FECHA DE CADUCIDAD </label>
    <input class="form-control" value="<?php echo $alimento->fcaducidad_ali; ?>" required="required" onkeypress="return validar(event)" type="date" name="fcaducidad_ali" id="fcaducidad_ali" placeholder="ejemplo: Seleccione">
    <br>
    <label for="">PRECIO </label>
    <input class="form-control" value="<?php echo $alimento->precio_ali; ?>" required="required"  type="text" name="precio_ali" id="precio_ali" placeholder="Ingrese el precio $">

    <label for="">TALLA DEL PERRO </label>
    <select style="color:#000000" class="form-control" required="requiered" name="tallaperro_ali" id="tallaperro_ali">
        <option value="">--Seleccione--</option>
        <option value="Grande">Grande</option>
        <option value="Mediano">Mediano</option>
        <option value="Pequeño">Pequeño</option>
    </select>
    <br>
    <label for="">PESO DEL PERRO </label>
    <input class="form-control" value="<?php echo $alimento->pesoperro_ali; ?>" required="required"  type="text" name="pesoperro_ali" id="pesoperro_ali" placeholder="Ingrese el peso de Can en KG">
    <br>
    <label for="">CANTIDAD </label>
    <input class="form-control" value="<?php echo $alimento->cantidad_ali; ?>" required="required"  type="number" name="cantidad_ali" id="cantidad_ali" placeholder="Ingrese la cantidad de alimento en stock ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/alimentos/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</form>
</div>
</div>
</div>

<script type="text/javascript">
  $("#tipo_ali").val("<?php echo $alimento->tipo_ali; ?>");
  $("#tallaperro_ali").val("<?php echo $alimento->tallaperro_ali; ?>");
</script>
