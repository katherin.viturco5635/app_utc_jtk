<br>
<center>
  <hr>
  <h2>ALIMENTO</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/alimentos/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i> Agregar un nuevo alimento
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover " id="tbl-alimentos">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CÓDIGO</th>
        <th class="text-center">MARCA</th>
        <th class="text-center">TIPO</th>
        <th class="text-center">INGREDIENTES</th>
        <th class="text-center">FECHA ELABORACIÓN</th>
        <th class="text-center">FECHA CADUCIDAD</th>
        <th class="text-center">PRECIO</th>
        <th class="text-center">TALLA DE PERROS</th>
        <th class="text-center">PESO PERROS</th>
        <th class="text-center">CANTIDAD</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()as $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->codigo_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->marca_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->tipo_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->ingredientes_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->felaboracion_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->fcaducidad_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->precio_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->tallaperro_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->pesoperro_ali;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->cantidad_ali;?>
        </td>
        <td class="text-center">
          <a href="<?php echo site_url(); ?>/alimentos/editar/<?php echo $filaTemporal->id_ali;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_ali; ?>');" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron alimentos</h3>

  </div>

<?php endif; ?>

<script type="text/javascript">
  function confirmarEliminacion(id_ali){
        iziToast.question({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Esta seguro de eliminar los datos de alimentos?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=
                    "<?php echo site_url(); ?>/alimentos/procesarEliminacion/"+id_ali;

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
  }
</script>

<script type="text/javascript">
$("#tbl-alimentos").DataTable({
  dom: 'lBfrtip',
  buttons: [
    'coppy','csv','excel','pdf','print'
  ]
});

</script>
