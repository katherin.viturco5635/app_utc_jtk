<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/alimentos/guardarAlimento"  method="post" id="frm_nuevo_alimento" >
  <hr>
  <center>
  <h1>DATOS DE LOS ALIMENTOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CODIGO DEL ALIMENTO </label>
    <input class="form-control" required="required"   type="text" name="codigo_ali" id="codigo_ali" placeholder="Registre el código. ejm: jgh-dfd5-h">
    <br>
    <label for=""> MARCA DEL ALIMENTO </label>
    <input
     class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="marca_ali" id="marca_ali" placeholder="Ingrese la marca de alimento, ejm:PROCAN">
     <br>
     <label for="">TIPO DE ALIMENTO: </label>
     <select class="form-control" name="tipo_ali" id="tipo_ali">
         <option value="">--Seleccione--</option>
         <option value="Seco">Seco</option>
         <option value="Húmedo">Húmedo</option>
         <option value="Galletas">Galletas</option>
         <option value="Huesos">Huesos</option>
     </select>
     <br>
    <label for=""> INGREDIENTES: </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="ingredientes_ali" id="ingredientes_ali" placeholder="Registre: Ejm, pollo,zanahoria">
    <br>
    <label for="">FECHA DE ELABORACION </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="felaboracion_ali" id="felaboracion_ali" placeholder="ejemplo: Seleccione">
    <br>
    <label for="">FECHA DE CADUCIDAD </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fcaducidad_ali" id="fcaducidad_ali" placeholder="ejemplo: Seleccione">
    <br>
    <label for="">PRECIO </label>
    <input class="form-control" required="required"  type="text" name="precio_ali" id="precio_ali" placeholder="Ingrese el precio $">
    <br>
    <label for="">TALLA DEL PERRO </label>
    <select class="form-control" name="tallaperro_ali" id="tallaperro_ali">
        <option value="">--Seleccione--</option>
        <option value="Grande">Grande</option>
        <option value="Mediano">Mediano</option>
        <option value="Pequeño">Pequeño</option>
    </select>
    <br>
    <label for="">PESO DEL PERRO </label>
    <input class="form-control" required="required"  type="text" name="pesoperro_ali" id="pesoperro_ali" placeholder="Ingrese el peso de Can en KG">
    <br>
    <label for="">CANTIDAD </label>
    <input class="form-control" required="required"  type="number" name="cantidad_ali" id="cantidad_ali" placeholder="Ingrese la cantidad de alimento en stock ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"><i class="fa fa-save "></i>GUARDAR</button>
    &nbsp;&nbsp;&nbsp;
<<<<<<< HEAD
    <a href="<?php echo site_url(); ?>/alimentos/index"   class="btn btn-warning"><i class="fa fa-times"></i>  CANCELAR
    </a>
  </form>
=======
    <button href="<?php echo base_url(); ?>/alimento/index"  type="submit" name="button" class="btn btn-primary"> <i class="fa fa-cancel "></i> CANCELAR</button>
</form>
>>>>>>> a238a485d9f96af6a4c160bfa8f949f4498d3b1d
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_alimento").validate({
      rules:{
        codigo_ali:{
          required:true
        },
        marca_ali:{
          letras:true,
          required:true
        },
        tipo_ali:{
          required:true
        },
        ingredientes_ali:{
          letras:true,
          required:true
        },
        felaboracion_ali:{
          required:true
        },
        fcaducidad_ali:{
          required:true
        },
        precio_ali:{
          required:true,
          minlength:1,
          maxlength:3,
          digits:true
        },
        tallaperro_ali:{
          required:true
        },
        pesoperro_ali:{
          required:true
        },
        cantidad_ali:{
          required:true,
          minlength:1,
          maxlength:3,
          digits:true
        },

      },

      messages:{
        codigo_ali:{
          required:"Por favor ingrese el código"
        },
        marca_ali:{
          letras:"Solo se pueden ingresar letras.",
          required:"Por favor ingrese la marca del alimento."
        },
        tipo_ali:{
          required:"Por favor seleccione el tipo de alimento."
        },
        ingredientes_ali:{
          letras:"En los ingredientes solo debe ingresar letras",
          required:"Por favor ingrese los ingredientes"
        },
        felaboracion_ali:{
          required:"Seleccione la fecha de elaboración."
        },
        fcaducidad_ali:{
          required:"Seleccione la fecha de caducidad."
        },
        precio_ali:{
          required:"Ingrese el precio del alimento.",
          minlength:"El precio debe ser mínimo de 1 número.",
          maxlength:"El precio debe ser máximo de 3 números.",
          digits:"El precio solo acepta números."
        },
        tallaperro_ali:{
          required:"Registre la talla del paciente."
        },
        pesoperro_ali:{
          required:"Registre el peso del paciente."
        },
        cantidad_ali:{
          required:"Ingrese la cantidad de alimento disponible",
          minlength:"La cantidad mínima es de i número.",
          maxlength:"La cantidad máxima es de 3 números.",
          digits:"La cantidad solo acepta números."
        }
      }
    });
</script>
