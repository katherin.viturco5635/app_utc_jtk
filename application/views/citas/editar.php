<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/citas/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_cit" id="id_cit"value="<?php echo $cita->id_cit; ?>">
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">

  <hr>
  <center>
  <h1>EDITAR CITA:</h1>
  <hr>
  </center>
    <br>
    <br>
    <label for=""> NOMBRES DEL PACIENTE: </label>
    <input class="form-control" value="<?php echo $cita->nombresp_cit; ?>" required="required"  onkeypress="return validar(event)" type="text" name="nombresp_cit" id="nombresp_cit" placeholder="ejemplo: Angel Amores">
    <br>
    <label for=""> NOMBRE DEL DUEÑO: </label>
    <input
     class="form-control" value="<?php echo $cita->nombresd_cit; ?>" required="required" onkeypress="return validar(event)"  type="text" name="nombresd_cit" id="nombresd_cit" placeholder="ejemplo: Monta Almachi">
     <br>
     <label for="">HISTORIA CLÍNICA: </label>
     <input class="form-control" value="<?php echo $cita->historia_cit; ?>" required="required" type="number" name="historia_cit" id="historia_cit" placeholder="ejemplo: 7856">
     <br>
     <label for="">ESPECIALIDAD:</label>
     <select class="form-control" style="color:#000000" name="especialidad_cit" id="especialidad_cit">
         <option value="">--Seleccione--</option>
         <option value="Oftalmología">Oftalmología</option>
         <option value="Oncología">Oncología</option>
         <option value="El lenguaje de los gatos ">El lenguaje de los gatos</option>
         <option value="Neurología">Neurología</option>
         <option value="Dermatología">Dermatología</option>
         <option value="Medicina de felinos">Medicina de felinos</option>
         <option value="Patología">Patología</option>
         <option value="Odontología">Odontología</option>

     </select>
     <br>
    <label for=""> FECHA: </label>
    <input class="form-control" value="<?php echo $cita->fecha_cit; ?>" required="required"   type="date" name="fecha_cit" id="fecha_cit" placeholder="ejemplo: 7:00 am">
    <br>
    <label for=""> HORA: </label>
    <input class="form-control" value="<?php echo $cita->horario_cit; ?>" required="required"  type="time" name="horario_cit" id="horario_cit" placeholder="ejemplo: 10:00 am">
    <br>
    <label for="">NIVEL DE URGENCIA:</label>
    <select class="form-control" style="color:#000000" name="nivelu_cit" id="nivelu_cit">
        <option value="">--Seleccione--</option>
        <option value="Alta">Alta</option>
        <option value="Media">Media</option>
        <option value="Baja">Baja</option>
    </select>
    <br>
    <label for="">PRÓPOSITO: </label>
    <input class="form-control" value="<?php echo $cita->proposito_cit; ?>" required="required" onkeypress="return validar(event)" type="text" name="proposito_cit" id="proposito_cit" placeholder="Ingrese el propósito de su cita">
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/citas/index"
      class="btn btn-warning">
      <i class="fa fa-times"></i>CANCELAR
    </a>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
  //Sctivando el pais selecionado para el clientes
  $("#especialidad_cit").val("<?php echo $cita->especialidad_cit; ?>");
  $("#nivelu_cit").val("<?php echo $cita->nivelu_cit; ?>");
</script>
