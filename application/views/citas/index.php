<br>
<center>
  <hr>
  <h2>CITAS REGISTRADAS </h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/citas/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar Nuevo
    </a>
    <br>
    <br>
</center>

<?php if ($listadoCitas): ?>
  <table class="table" id="tbl-citas">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRES PACIENTE</th>
        <th class="text-center">NOMBRES DUEÑO</th>
        <th class="text-center">HISTORIA CLÍNICA</th>
        <th class="text-center">ESPECIALIDAD</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">HORARIO</th>
        <th class="text-center">NIVEL DE URGENCIA</th>
        <th class="text-center">PROPÓSITO</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoCitas->result() as $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombresp_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombresd_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->historia_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->especialidad_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->fecha_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->horario_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nivelu_cit;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->proposito_cit;?>
        </td>

        <td class="text-center">



          <a href="<?php echo site_url(); ?>/citas/editar/<?php echo $filaTemporal->id_cit;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)"
              onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_cit; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>
  </table>

  <script type="text/javascript">

  $('#tbl-citas').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );

  </script>


<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron CITAS por el momento</h3>

  </div>

<?php endif; ?>

<script type="text/javascript">
          function confirmarEliminacion(id_cit){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar la cita de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/citas/procesarEliminacion/"+id_cit;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
      $("#tbl-citas").DataTable();
        </script>
