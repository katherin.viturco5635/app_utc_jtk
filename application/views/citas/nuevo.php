<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/citas/guardarCita"
  method="post" id="frm_nuevo_cita"
  >
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <hr>
  <center>
  <h1>REGISTRE SU CITA:</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> NOMBRES DEL PACIENTE: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="nombresp_cit" id="nombresp_cit" placeholder="ejemplo: Angel Amores">
    <br>
    <label for=""> NOMBRE DEL DUEÑO: </label>
    <input
     class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="nombresd_cit" id="nombresd_cit" placeholder="ejemplo: Monta Almachi">
     <br>
     <label for="">HISTORIA CLÍNICA: </label>
     <input class="form-control" required="required" type="number" name="historia_cit" id="historia_cit" placeholder="ejemplo: 7856">
     <br>
     <label for="">ESPECIALIDAD:</label>
     <select class="form-control" name="especialidad_cit" id="especialidad_cit">
         <option value="">--Seleccione--</option>
         <option value="Oftalmología">Oftalmología</option>
         <option value="Oncología">Oncología</option>
         <option value="El lenguaje de los gatos ">El lenguaje de los gatos</option>
         <option value="Neurología">Neurología</option>
         <option value="Dermatología">Dermatología</option>
         <option value="Medicina de felinos">Medicina de felinos</option>
         <option value="Patología">Patología</option>
         <option value="Odontología">Odontología</option>

     </select>
     <br>
    <label for=""> FECHA: </label>
    <input class="form-control" required="required"   type="date" name="fecha_cit" id="fecha_cit" placeholder="ejemplo: 7:00 am">
    <br>
    <label for=""> HORA: </label>
    <input class="form-control" required="required"  type="time" name="horario_cit" id="horario_cit" placeholder="ejemplo: 10:00 am">
    <br>
    <label for="">NIVEL DE URGENCIA:</label>
    <select class="form-control" name="nivelu_cit" id="nivelu_cit">
        <option value="">--Seleccione--</option>
        <option value="Alta">Alta</option>
        <option value="Media">Media</option>
        <option value="Baja">Baja</option>
    </select>
    <br>
    <label for="">PRÓPOSITO: </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="proposito_cit" id="proposito_cit" placeholder="Ingrese el propósito de su cita">
    <br><br>
    <button type="submit" name="button" class="btn btn-info">  <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/citas/index"
      class="btn btn-warning"> <i class="fa fa-times "></i> CANCELAR
    </a>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_cita").validate({
      rules:{
        nombresp_cit:{
          letras:true,
          required:true
        },
        nombresd_cit:{
          letras:true,
          required:true
        },
        historia_cit:{
          required:true,
          minlength:1,
          maxlength:5,
          digits:true
        },
        especialidad_cit:{
          required:true
        },
        fecha_cit:{
          required:true
        },
        horario_cit:{
          required:true
        },
        nivelu_cit:{
        required:true
        },
        proposito_cit:{
        letras:true,
        required:true
      }

      },
      messages:{
        nombresp_cit:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el nombre del paciente solo con letras"
        },
        nombresd_cit:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el nombre del dueño solo con letras"
        },
        historia_cit:{
          required:"Por favor ingrese la historia clínica en números",
          minlength:"El número de historia debe tener mínimo 1 digitos",
          maxlength:"El número de historia debe tener máximo 5 digitos",
          digits:"El número de historia solo acepta números"
        },
        especialidad_cit:{
          required:"Por favor seleccione la especialidad"
        },
        fecha_cit:{
          required:"Por favor seleccione la fecha"
        },
        horario_cit:{
          required:"Por favor seleccione la hora"
        },
        nivelu_cit:{
        required:"Por favor seleccione el nivel de urgencia"
        },
        proposito_cit:{
        letras:"Ingrese solamente letras",
        required:"Ingrese el propósito del paciente solo con letras"
      }
    }

  });
</script>
