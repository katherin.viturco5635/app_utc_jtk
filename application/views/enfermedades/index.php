<br>
<center>
  <hr>
  <h2>ENFERMEDADES</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/enfermedades/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i> Agregar una nueva enfermedad
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DE LA ENFERMEDAD</th>
        <th class="text-center">DESCRIPCION</th>
        <th class="text-center">TRATAMIENTO</th>

      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
            <?php echo $filaTemporal->id_enf; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_enf; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->descripcion_enf; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->tratamiento_enf; ?>
          </td>

        <td class="text-center">
          <a href="" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
          <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/enfermedades/procesarEliminacion/<?php echo $filaTemporal->id_enf;?>" class="btn btn-danger"> <i class="fa fa-trash"></i> </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron enfermedades</h3>

  </div>

<?php endif; ?>
