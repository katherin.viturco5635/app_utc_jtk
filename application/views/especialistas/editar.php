<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/especialistas/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_esp" id="id_esp"value="<?php echo $especialista->id_esp; ?>">
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">

  <hr>
  <center>
  <h1>REGISTRE SU INFORMACIÓN:</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> IDENTIFICACIÓN: </label>
    <input class="form-control" value="<?php echo $especialista->identificacion_esp; ?>" required="required" type="number" name="identificacion_esp" id="identificacion_esp" placeholder="ejemplo: 0504698559">
    <br>
    <label for=""> NOMBRES: </label>
    <input
     class="form-control" value="<?php echo $especialista->nombres_esp; ?>" required="required" onkeypress="return validar(event)"  type="text" name="nombres_esp" id="nnombres_esp" placeholder="ejemplo: Juan Carlos">
     <br>
     <label for=""> APELLIDOS: </label>
     <input
      class="form-control" value="<?php echo $especialista->apellidos_esp; ?>" required="required" onkeypress="return validar(event)"  type="text" name="apellidos_esp" id="apellidos_esp" placeholder="ejemplo: Monta Almachi">
      <br>
      <label for=""> FECHA DE NACIMIENTO: </label>
      <input class="form-control" value="<?php echo $especialista->fechan_esp; ?>" required="required"   type="date" name="fechan_esp" id="fechan_esp">
      <br>
     <label for="">EDAD: </label>
     <input class="form-control" value="<?php echo $especialista->edad_esp; ?>" required="required" type="number" name="edad_esp" id="edad_esp" placeholder="ejemplo: 7">
     <br>
    <label for=""> DIRECCIÓN: </label>
    <input class="form-control" value="<?php echo $especialista->direccion_esp; ?>" required="required"  onkeypress="return validar(event)" type="text" name="direccion_esp" id="direccion_esp" placeholder="ejemplo: Latacunga Guaytacama">
    <br>
    <label for=""> TELÉFONO: </label>
    <input class="form-control" value="<?php echo $especialista->telefono_esp; ?>" required="required"  type="number" name="telefono_esp" id="telefono_esp" placeholder="ejemplo: 0981234567">
    <br>
    <label for=""> TIPO DE SANGRE: </label>
    <input class="form-control" value="<?php echo $especialista->tipos_esp; ?>" required="required"  onkeypress="return validar(event)" type="text" name="tipos_esp" id="tipos_esp" placeholder="ejemplo: O positivo">
    <br>
    <label for=""> ESPECIALIDAD/ES: </label>
    <input class="form-control" value="<?php echo $especialista->especialidad_esp; ?>" required="required"  onkeypress="return validar(event)" type="text" name="especialidad_esp" id="especialidad_esp" placeholder="ejemplo: Odontólogo">
    <br>
    <label for=""> NACIONALIDAD: </label>
    <input class="form-control" value="<?php echo $especialista->nacionalidad_esp; ?>" required="required"  onkeypress="return validar(event)" type="text" name="nacionalidad_esp" id="nacionalidad_esp" placeholder="ejemplo: Ecuatoriano">
    <br>
    <label for="">ESTADO CIVIL:</label>
    <select class="form-control" style="color:#000000" name="estado_esp" id="estado_esp">
        <option value="">--Seleccione--</option>
        <option value="Casado">Casado</option>
        <option value="Soltero">Soltero</option>
        <option value="Divorciado">Divorciado</option>
        <option value="Viudo">Viudo</option>
    </select>
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/especialistas/index"
      class="btn btn-warning"> <i class="fa fa-times "></i> CANCELAR
    </a>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
  //Sctivando el pais selecionado para el clientes
  $("#estado_esp").val("<?php echo $especialistas->estado_esp; ?>");
</script>
