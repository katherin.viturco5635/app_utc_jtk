<br>
<center>
  <hr>
  <h2>ESPECIALISTAS REGISTRADOS </h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/especialistas/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar Nuevo
    </a>
    <br>
    <br>
</center>

<?php if ($listadoEspecialistas): ?>
  <table class="table " id="tbl-especialistas">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">IDENTIFICACIÓN</th>
        <th class="text-center">NOMBRES</th>
        <th class="text-center">APELLIDOS</th>
        <th class="text-center">FECHA DE NACIMIENTO</th>
        <th class="text-center">EDAD</th>
        <th class="text-center">DIRECCIÓN</th>
        <th class="text-center">TELÉFONO</th>
        <th class="text-center">TIPOS DE SANGRE</th>
        <th class="text-center">ESPECIALIDAD</th>
        <th class="text-center">NACIONALIDAD</th>
        <th class="text-center">ESTADO CIVIL</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoEspecialistas->result() as $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->identificacion_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombres_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->apellidos_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->fechan_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->edad_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->direccion_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->telefono_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->tipos_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->especialidad_esp;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nacionalidad_esp ;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->estado_esp ;?>
        </td>
        <td class="text-center">

          <a href="<?php echo site_url(); ?>/especialistas/editar/<?php echo $filaTemporal->id_esp;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)"
              onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_esp; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>
  </table>

  <script type="text/javascript">

  $('#tbl-especialistas').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );

  </script>


<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron ESPECIALISTAS REGISTRADOS por el momento </h3>

  </div>

<?php endif; ?>

<script type="text/javascript">
          function confirmarEliminacion(id_esp){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar al especialista de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/especialistas/procesarEliminacion/"+id_esp;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
      $("#tbl-especialistas").DataTable();
        </script>
