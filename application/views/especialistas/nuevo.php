<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/especialistas/guardarEspecialista"
  method="post" id="frm_nuevo_especialista"
  >
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <hr>
  <center>
  <h1>REGISTRE SU INFORMACIÓN:</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> IDENTIFICACIÓN: </label>
    <input class="form-control" required="required" type="number" name="identificacion_esp" id="identificacion_esp" placeholder="ejemplo: 0504698559">
    <br>
    <label for=""> NOMBRES: </label>
    <input
     class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="nombres_esp" id="nombres_esp" placeholder="ejemplo: Juan Carlos">
     <br>
     <label for=""> APELLIDOS: </label>
     <input
      class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="apellidos_esp" id="apellidos_esp" placeholder="ejemplo: Monta Almachi">
      <br>
      <label for=""> FECHA DE NACIMIENTO: </label>
      <input class="form-control" required="required"   type="date" name="fechan_esp" id="fechan_esp">
      <br>
     <label for="">EDAD: </label>
     <input class="form-control" required="required" type="number" name="edad_esp" id="edad_esp" placeholder="ejemplo: 7">
     <br>
    <label for=""> DIRECCIÓN: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="direccion_esp" id="direccion_esp" placeholder="ejemplo: Latacunga Guaytacama">
    <br>
    <label for=""> TELÉFONO: </label>
    <input class="form-control" required="required"  type="number" name="telefono_esp" id="telefono_esp" placeholder="ejemplo: 0981234567">
    <br>
    <label for=""> TIPO DE SANGRE: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="tipos_esp" id="tipos_esp" placeholder="ejemplo: O positivo">
    <br>
    <label for=""> ESPECIALIDAD/ES: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="especialidad_esp" id="especialidad_esp" placeholder="ejemplo: Odontólogo">
    <br>
    <label for=""> NACIONALIDAD: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="nacionalidad_esp" id="nacionalidad_esp" placeholder="ejemplo: Ecuatoriano">
    <br>
    <label for="">ESTADO CIVIL:</label>
    <select class="form-control" name="estado_esp" id="estado_esp">
        <option value="">--Seleccione--</option>
        <option value="Casado">Casado</option>
        <option value="Soltero">Soltero</option>
        <option value="Divorciado">Divorciado</option>
        <option value="Viudo">Viudo</option>
    </select>
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/especialistas/index"
      class="btn btn-warning"> <i class="fa fa-times "></i> CANCELAR
    </a>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_especialista").validate({
      rules:{
        identificacion_esp:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombres_esp:{
          letras:true,
          required:true
        },
        apellidos_esp:{
          letras:true,
          required:true
        },
        fechan_esp:{
          required:true
        },
        edad_esp:{
          required:true,
          minlength:1,
          maxlength:2,
          digits:true
        },
        direccion_esp:{
          letras:true,
          required:true
        },
        telefono_esp:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        tipos_esp:{
          letras:true,
          required:true
        },
        especialidad_esp:{
        letras:true,
        required:true
        },
        nacionalidad_esp:{
        letras:true,
        required:true
      },
      estado_esp:{
        required:true
      }

      },
      messages:{
        identificacion_esp:{
          required:"Por favor ingrese la cédula en números",
          minlength:"El número de edad debe tener mínimo 10 digitos",
          maxlength:"El número de edad debe tener máximo 10 digitos",
          digits:"El número de cédula solo acepta números"
        },
        nombres_esp:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el nombre solo con letras"
        },
        apellidos_esp:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el apellido solo con letras"
        },
        fechan_esp:{
          required:"Por favor seleccione la fecha de nacimiento"
        },
        edad_esp:{
          required:"Por favor ingrese la edad en números",
          minlength:"El número de edad debe tener mínimo 1 digitos",
          maxlength:"El número de edad debe tener máximo 2 digitos",
          digits:"El número de edad solo acepta números"
        },
        direccion_esp:{
          letras:"Ingrese solamente letras",
          required:"Ingrese la dirección solo con letras"
        },
        telefono_esp:{
          required:"Por favor ingrese el número del teléfono ",
          minlength:"El número de teléfono debe tener mínimo 10 digitos",
          maxlength:"El número de teléfono debe tener máximo 10 digitos",
          digits:"El número de teléfono solo acepta números"
        },
        tipos_esp:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el tipo de sangre solo con letras"
        },
        especialidad_esp:{
        letras:"Ingrese solamente letras",
        required:"Ingrese el tipo de especialidad solo con letras"
        },
        nacionalidad_esp:{
        letras:"Ingrese solamente letras",
        required:"Ingrese el tipo de nocionalidad solo con letras"
        },
        estado_esp:{
        required:"Por favor seleccione el estado civil"
        }
      }
    });
</script>
