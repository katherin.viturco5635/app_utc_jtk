<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/historias/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_his" id="id_his" value="<?php echo $historia->id_his;?>">
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <hr>
  <center>
  <h1>REGISTRE LA HISTORIA CLÍNICA:</h1>
  <hr>
  </center>

    <br>
    <br>
    <br>
    <label for="">NOMBRES: </label>
      <input class="form-control" value="<?php echo $historia->nombre_pac ?>" required="required"  type="text" name="nombre_pac" id="nombre_pac" readonly >
   <label for=""> FECHA: </label>
   <input class="form-control" value="<?php echo $historia->fecha_his; ?>" required="required"   type="date" name="fecha_his" id="fecha_his">
   <br>
    <label for=""> EMFERMEDAD: </label>
    <input class="form-control" value="<?php echo $historia->enfermedad_his; ?>" required="required"  onkeypress="return validar(event)" type="text" name="enfermedad_his" id="enfermedad_his">
    <br>
    <label for=""> TIEMPO: </label>
    <input class="form-control" value="<?php echo $historia->tiempo_his; ?>" required="required" type="time" name="tiempo_his " id="tiempo_his ">
     <br>
      <label for="">SÍNTOMAS: </label>
      <input class="form-control" value="<?php echo $historia->sintomas_his; ?>"  required="required" onkeypress="return validar(event)" type="text"name="sintomas_his" id="sintomas_his" placeholder="Ingrese los sintomas que el paciente presente">
      <br>
    <label for=""> PESO: </label>
    <input class="form-control" value="<?php echo $historia->peso_his; ?>" required="required"  type="number" name="peso_his" id="peso_his" placeholder="Ingrese el peso del paciente">
    <br>
    <label for="">TALLA: </label>
    <input class="form-control" value="<?php echo $historia->talla_his; ?>" required="required" type="number" name="talla_his" id="talla_his" placeholder="Ingrese la talla del paciente">
    <br>
    <label for=""> OTRAS ENFERMEDADES: </label>
    <input
     class="form-control" value="<?php echo $historia->otrae_his; ?>" required="required" onkeypress="return validar(event)"  type="text" name="otrae_his" id="otrae_his" placeholder="Si el paciente presenta otras enfermedades ingreselo">
     <br>
     <label for="">MOTIVO: </label>
     <input class="form-control" value="<?php echo $historia->motivo_his; ?>" required="required" onkeypress="return validar(event)" type="text" name="motivo_his" id="motivo_his" placeholder="Ingrese motivos de la consulta">
     <br>
     <br>
     <button type="submit" name="button" class="btn btn-primary">
       ACTUALIZAR
     </button>
     &nbsp;&nbsp;&nbsp;
     <a href="<?php echo site_url(); ?>/historias/index"
       class="btn btn-warning">
       <i class="fa fa-times"></i>CANCELAR
     </a>
 </div>
</form>
</div>
</div>
</div>
