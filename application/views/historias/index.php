<br>
<center>
  <hr>
  <h2>HISTORIAS CLÍNICAS REGISTRADAS </h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/historias/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar Nuevo
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">PACIENTE</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">ENFERMEDAD</th>
        <th class="text-center">TIEMPO</th>
        <th class="text-center">SÍNTOMAS</th>
        <th class="text-center">PESO</th>
        <th class="text-center">TALLA</th>
        <th class="text-center">OTRAS ENFERMEDADES</th>
        <th class="text-center">MOTIVO</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_pac;?>
        <td class="text-center">
          <?php echo $filaTemporal->fecha_his ;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->enfermedad_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->tiempo_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->sintomas_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->peso_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->talla_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->otrae_his;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->motivo_his;?>
        </td>

        <td class="text-center">
          <a href="<?php echo site_url(); ?>/historias/editar/<?php echo $filaTemporal->id_his;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)"
              onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_his; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>
  </table>

  <script type="text/javascript">

  $('#tbl-historias').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );

  </script>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron HISTORIAS CLÍNICAS</h3>

  </div>

<?php endif; ?>

<script type="text/javascript">
          function confirmarEliminacion(id_his){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar la historia del paciente de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/historias/procesarEliminacion/"+id_his;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
      $("#tbl-historias").DataTable();
        </script>
