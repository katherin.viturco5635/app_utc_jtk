<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/historias/guardarHistoria"
  method="post" id="frm_nuevo_historia" >
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">

  <hr>
  <center>
  <h1>REGISTRE LA HISTORIA CLÍNICA:</h1>
  <hr>
  </center>

    <br>
    <br>
    <br>
   <label for="form-control"> FECHA: </label>

   <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fecha_his" id="fecha_his" placeholder="ejemplo: Seleccione">
   <br>
<!-- cargar los datos del paciente en un select -->

       <select class="form-control" name="fk_id_pac" id="fk_id_pac">
<?php foreach ($listadoPacientes->result() as $valor): ?>
     <option value="<?php echo $valor->id_pac ?>"><?php echo $valor->nombre_pac ?></option>
 <?php endforeach; ?>
   </select>
<br>

    <label for="form-control"> EMFERMEDAD: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="enfermedad_his" id="enfermedad_his" placeholder="Ingrese la enfermadad del paciente">
    <br>
    <label for="form-control"> TIEMPO: </label>
    <input
     class="form-control" required="required" type="time" name="tiempo_his" id="tiempo_his">
     <br>
      <label for="">SÍNTOMAS: </label>
      <input class="form-control" required="required" onkeypress="return validar(event)" type="text"name="sintomas_his" id="sintomas_his" placeholder="Ingrese los sintomas que el paciente presente">
      <br>
    <label for=""> PESO: </label>
    <input class="form-control" required="required"  type="number" name="peso_his" id="peso_his" placeholder="Ingrese el peso del paciente">
    <br>
    <label for="">TALLA: </label>
    <input class="form-control" required="required" type="number" name="talla_his" id="talla_his" placeholder="Ingrese la talla del paciente">
    <br>
    <label for=""> OTRAS ENFERMEDADES: </label>
    <input
     class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="otrae_his" id="otrae_his" placeholder="Si el paciente presenta otras enfermedades ingreselo">
     <br>
     <label for="">MOTIVO: </label>
     <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="motivo_his" id="motivo_his" placeholder="Ingrese motivos de la consulta">
     <br>
     <br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/historias/index"
      class="btn btn-warning"> <i class="fa fa-times "></i> CANCELAR
    </a>

</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_historia").validate({
      rules:{
        fecha_his:{
          required:true
        },
        enfermedad_his:{
          letras:true,
          required:true
        },
        tiempo_his:{
          required:true
        },
        sintomas_his:{
          letras:true,
          required:true
        },
        peso:{
          required:true,
          minlength:1,
          maxlength:5,
          digits:true
        },
        talla_his:{
          required:true,
          minlength:1,
          maxlength:2,
          digits:true
        },
        enfermedad_his:{
        rletras:true,
        required:true
        },
        motivo_his:{
        letras:true,
        required:true
      }

      },
      messages:{
        fecha_his:{
          required:"Por favor seleccione la fecha"
        },
        enfermedad_his:{
          letras:"Ingrese solamente letras",
          required:"Ingrese la enfermedad solo con letras"
        },
        tiempo_his:{
          required:"Por favor seleccione la hora"
        },
        sintomas_his:{
          letras:"Ingrese solamente letras",
          required:"Ingrese los sintomas solo con letras"
        },
        peso:{
          required:"Por favor ingrese la historia clínica en números",
          minlength:"El número de historia debe tener mínimo 1 digitos",
          maxlength:"El número de historia debe tener máximo 4 digitos",
          digits:"El número de historia solo acepta números"
        },
        talla_his:{
          required:"Por favor ingrese la historia clínica en números",
          minlength:"El número de historia debe tener mínimo 1 digitos",
          maxlength:"El número de historia debe tener máximo 2 digitos",
          digits:"El número de historia solo acepta números"
        },
        enfermedad_his:{
          letras:"Ingrese solamente letras",
          required:"Ingrese la enfermadad solo con letras"
        },
        motivo_his:{
          letras:"Ingrese solamente letras",
          required:"Ingrese los motivos solo con letras"
      }
    }

  });
</script>
