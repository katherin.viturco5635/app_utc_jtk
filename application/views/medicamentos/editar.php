<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/medicamentos/procesarActualizacion"  method="post"  >

    <input type="hidden" name="id_med" id="id_med" value="<?php echo $medicamento->id_med; ?>">
    <hr>
  <center>
  <h1>ACTUALIZAR DATOS DE LOS MEDICAMENTOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CODIGO DEL MEDICAMENTO </label>
    <input class="form-control" value="<?php echo $medicamento->codigo_med; ?>" required="required"   type="text" name="codigo_med" id="codigo_med" placeholder="ejemplo: jgh-dfd5-h">
    <br>
    <label for=""> NOMBRE DEL MEDICAMENTO</label>
    <input class="form-control" value="<?php echo $medicamento->nombre_med; ?>" required="required" onkeypress="return validar(event)"  type="text" name="nombre_med" id="nombre_med" placeholder="ejemplo:Vitaminas">
     <br>
    <label for=""> LOTE  </label>
    <input class="form-control" value="<?php echo $medicamento->lote_med; ?>" required="required"  type="text" name="lote_med" id="lote_med" placeholder="ejemplo: VZ25478J2">
    <br>
    <label for="">DOSIS </label>
    <input class="form-control" value="<?php echo $medicamento->dosis_med; ?>" required="required"  type="text" name="dosis_med" id="dosis_med" placeholder="ejemplo: 15 ml">
    <br>
    <label for="">FECHA DE ELABORACIÓN </label>
    <input class="form-control" value="<?php echo $medicamento->felaboracion_med; ?>" required="required" onkeypress="return validar(event)" type="date" name="felaboracion_med" id="felaboracion_med" placeholder="Seleccione">
    <br>
    <label for="">FECHA DE VENCIMIENTO</label>
    <input class="form-control" value="<?php echo $medicamento->fvencimiento_med; ?>" required="required" onkeypress="return validar(event)" type="date" name="fvencimiento_med" id="fvencimiento_med" placeholder="Seleccione">
    <br>
    <label for="">CANTIDAD </label>
    <input class="form-control" value="<?php echo $medicamento->cantidad_med; ?>" required="required"  type="number" name="cantidad_med" id="cantidad_med" placeholder="ejemplo: 10 ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/medicamentos/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</form>
</div>
</div>
</div>
