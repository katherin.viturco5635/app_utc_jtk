<br>
<center>
  <hr>
  <h2>MEDICAMENTO</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/medicamentos/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar un nuevo medicamento
    </a>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover " id="tbl-medicamentos">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CÓDIGO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">LOTE</th>
        <th class="text-center">DOSIS</th>
        <th class="text-center">FECHA DE ELABORACIÓN</th>
        <th class="text-center">FECHA DE VENCIMIENTO</th>
        <th class="text-center">CANTIDAD</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->codigo_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->lote_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->dosis_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->felaboracion_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->fvencimiento_med;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->cantidad_med;?>
        </td>

        <td class="text-center">
          <a href="<?php echo site_url(); ?>/medicamentos/editar/<?php echo $filaTemporal->id_med;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_med; ?>');" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron medicamentos</h3>

  </div>

<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_med){
        iziToast.question({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Esta seguro de eliminar el medicamento de forma pernante?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=
                    "<?php echo site_url(); ?>/medicamentos/procesarEliminacion/"+id_med;

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
  }
</script>

<script type="text/javascript">
$("#tbl-medicamentos").DataTable({
  dom: 'lBfrtip',
  buttons: [
    'coppy','csv','excel','pdf','print'
  ]
});
</script>
