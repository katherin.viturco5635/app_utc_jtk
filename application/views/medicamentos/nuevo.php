<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/medicamentos/guardarMedicamento"  method="post" id="frm_nuevo_medicamento" >
  <hr>
  <center>
  <h1>DATOS DE LOS MEDICAMENTOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CODIGO DEL MEDICAMENTO </label>
    <input class="form-control" required="required"   type="text" name="codigo_med" id="codigo_med" placeholder="ejemplo: jgh-dfd5-h">
    <br>
    <label for=""> NOMBRE DEL MEDICAMENTO </label>
    <input class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="nombre_med" id="nombre_med" placeholder="ejemplo:Vitaminas">
     <br>
    <label for=""> LOTE  </label>
    <input class="form-control" required="required"  type="text" name="lote_med" id="lote_med" placeholder="ejemplo: VZ25478J2">
    <br>
    <label for="">DOSIS </label>
    <input class="form-control" required="required"  type="text" name="dosis_med" id="dosis_med" placeholder="ejemplo: 15 ml">
    <br>
    <label for="">FECHA DE ELABORACIÓN </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="felaboracion_med" id="felaboracion_med" placeholder="Seleccione">
    <br>
    <label for="">FECHA DE VENCIMIENTO</label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fvencimiento_med" id="fvencimiento_med" placeholder="Seleccione">
    <br>
    <label for="">CANTIDAD </label>
    <input class="form-control" required="required"  type="number" name="cantidad_med" id="cantidad_med" placeholder="ejemplo: 10 ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/medicamentos/index"   class="btn btn-warning"><i class="fa fa-times"></i>  CANCELAR </a>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_medicamento").validate({
      rules:{
        codigo_meds:{
          required:true
        },
        nombre_med:{
          letras:true,
          required:true
        },
        lote_med:{
          required:true
        },
        dosis_med:{
          required:true
        },
        felaboracion_med:{
          required:true
        },
        fvencimiento_med:{
          required:true
        },
        cantidad_med:{
          required:true,
          digits:true
        },
      },

      messages:{
        codigo_med:{
          required:"Ingrese el código del medicamento."
        },
        nombre_med:{
          letras:"El nombre solo debe tener letras.",
          required:"Por favor ingrese el nombre. "
        },
        lote_med:{
          required:"Por favor ingrese el lote del medicamento."
        },
        dosis_med:{
          required:"Por favor ingrese el número de dosis."
        },
        felaboracion_med:{
          required:"Por favor seleccione la fecha de elaboración"
        },
        fvencimiento_med:{
          required:"Por favor seleccione la fecha de vencimiento."
        },
        cantidad_med:{
          required:"Por favor ingrese la cantidad de medicamento disponible.",
          digits:"La cantidad solo acepta números."
        }
      }
    });
</script>
