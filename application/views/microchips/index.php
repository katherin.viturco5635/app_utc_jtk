<br>
<center>
  <hr>
  <h2>MICROCHIPS</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/microchips/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i> Agregar una nuevo microchip
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">TIPO DE MICROCHIP/th>
        <th class="text-center">CODIGO DE MICROCHIP</th>
        <th class="text-center">SERIAL DEL MICROCHIP</th>
        <th class="text-center">COLOR/th>
        <th class="text-center">TAMAÑO</th>
        <th class="text-center">FECHA DE EMISION</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
            <?php echo $filaTemporal->id_mic; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->tipo_mic; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->codigo_mic; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->serial_mic; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->color_mic; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->tamaño_mic; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->fechaemision_mic; ?>
          </td>
        <td class="text-center">
          <a href="" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
          <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/microchips/procesarEliminacion/<?php echo $filaTemporal->id_mic;?>" class="btn btn-danger"> <i class="fa fa-trash"></i> </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron microchip</h3>

  </div>

<?php endif; ?>
