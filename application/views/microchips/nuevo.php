<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/microchips/guardarmicrochip"
  method="post"
  >
  <hr>
  <center>
  <h1>DATOS DE MICROCHIPS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> TIPO DE MICROCHIP </label>
    <select class="form-control" name="tipo_mic" id="tipo_mic">
        <option value="">--Seleccione--</option>
        <option value="Seco">PASTILLA</option>
        <option value="Húmedo">COLLAR</option>
        <option value="Galletas">ARETE</option>
        <option value="Huesos">OTROS</option>
    </select>
    <br>
    <label for=""> CODIGO DEL MICROCHIP</label>
    <input
     class="form-control" required="required"   type="text" name="codigo_mic" id="codigo_mic" placeholder="Registre el codigo del microchip. ejm: a832yt.....">
     <br>
     <label for=""> SERIAL DEL MICROCHIP</label>
     <input
      class="form-control" required="required" onkeypress="return validar(event)"  type="number" name="serial_mic" id="serial_mic" placeholder="Ingrese el numero serial , ejm:2020">
      <br>
     <label for=""> COLOR </label>
     <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="color_mic" id="color_mic" placeholder="Registre el costso: Ejm, 6 ">

     <br>
    <label for=""> TAMAÑO </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="number" name="tamaño_mic" id="tamaño_mic" placeholder="Registre el costso: Ejm, 6 ">
    <br>
    <label for="">FECHA DE EMISION </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fechaemision_mic" id="fechaemision_mic" placeholder="ejemplo: Seleccione">
    <br>

    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <button href="<?php echo base_url(); ?>/adopcion/index"  type="submit" name="button" class="btn btn-primary"> <i class="fa fa-cancel "></i> CANCELAR</button>
</form>
</div>
</div>
</div>
