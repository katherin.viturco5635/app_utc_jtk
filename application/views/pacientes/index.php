<br>
<center>
  <hr>
  <h2>PACIENTES REGISTRADOS</h2>
</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/pacientes/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar Nuevo
    </a>
    <br>
    <br>
</center>

<?php if ($listadoPacientes): ?>
  <table class="table" id="tbl-pacientes">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRES</th>
        <th class="text-center">SEXO</th>
        <th class="text-center">EDAD</th>
        <th class="text-center">ESTERILIZADO</th>
        <th class="text-center">RAZA</th>
        <th class="text-center">COLOR</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoPacientes->result() as $filaTemporal): ?>
      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_pac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_pac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->sexo_pac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->edad_pac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->esterilizado_pac;?>
        </td>

        <td class="text-center">
          <?php echo $filaTemporal->raza_pac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->color_pac;?>
        </td>
        <td class="text-center">

          <a href="<?php echo site_url(); ?>/pacientes/editar/<?php echo $filaTemporal->id_pac;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)"
              onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_pac; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
          </a>
      </td>
      </tr>

      <?php endforeach; ?>
    </tbody>

  </table>

  <script type="text/javascript">

  $('#tbl-pacientes').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );

  </script>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron PACIENTES</h3>

  </div>

<?php endif; ?>

<script type="text/javascript">
          function confirmarEliminacion(id_pac){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar el paciente de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/pacientes/procesarEliminacion/"+id_pac;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
      $("#tbl-pacientes").DataTable();
        </script>
