<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/pacientes/guardarPaciente"
  method="post" id="frm_nuevo_paciente"
  >
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <center>
  <hr>
  <h3>REGISTRO DEL NUEVO PACIENTE:</h3>
  <hr>
  </center>
    <br>
    <br>
    <label for="">NOMBRES: </label>
<input class="form-control" required="required" onkeypress="return validar(event)"   type="text" name="nombre_pac" id="nombre_pac" placeholder="Por favor Ingrese el nombre del paciente">
    <br>
    <br>
    <label for="">SEXO:</label>
    <select class="form-control" name="sexo_pac" id="sexo_pac">
        <option value="">--Seleccione--</option>
        <option value="Macho">Macho</option>
        <option value="Hembra">Hebra</option>
    </select>
    <br>
    <br>
    <label for="">EDAD: </label>
    <input class="form-control" required="required" type="number" name="edad_pac" id="edad_pac" placeholder="Por favor Ingrese la edad del paciente">
    <br>
    <br>
    <label for="">ESTERILIZADO </label>
    <select class="form-control" name="esterilizado_pac" id="esterilizado_pac">
        <option value="">--Seleccione--</option>
        <option value="Si">Si</option>
        <option value="No">No</option>
    </select>
    <br>
    <br>
    <label for="">RAZA: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="raza_pac" id="raza_pac" placeholder="Por favor Ingrese la raza del paciente">
    <br>
    <br>
    <label for="">COLOR: </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="color_pac" id="color_pac" placeholder="Por favor Ingrese el color del paciente">
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/pacientes/index"
      class="btn btn-warning"> <i class="fa fa-times "></i> CANCELAR
    </a>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_paciente").validate({
      rules:{
        nombre_pac:{
          letras:true,
          required:true
        },
        sexo_pac:{
          required:true
        },
        edad_pac:{
          required:true,
          minlength:1,
          maxlength:2,
          digits:true
        },
        esterilizado_pac:{
          required:true
        },
        raza_pac:{
          letras:true,
          required:true
        },
        color_pac:{
          letras:true,
          required:true
      }
      },
      messages:{
        nombre_pac:{
        letras:"Ingrese solamente letras",
        required:"Ingrese el nombre solo con letras"
      },
      sexo_pac:{
        required:"Por favor seleccione el sexo"
      },
      edad_pac:{
        required:"Por favor ingrese la edad en números",
        minlength:"El número de edad debe tener mínimo 1 digitos",
        maxlength:"El número de edad debe tener máximo 2 digitos",
        digits:"El número de edad solo acepta números"
      },
      esterilizado_pac:{
        required:"Por favor seleccione esi es o no esterilizado"
      },
      raza_pac:{
        letras:"Ingrese solamente letras",
        required:"Ingrese la raza solo con letras"
      },
      color_pac:{
        letras:"Ingrese solamente letras",
        required:"Ingrese el color solo con letras"
        }
      }
    });
</script>
