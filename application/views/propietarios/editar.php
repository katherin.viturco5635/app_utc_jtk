<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/propietarios/procesarActualizacion"   method="post" >
   <input type="hidden" name="id_pro" id="id_pro" value="<?php echo $propietario->id_pro; ?>">
  <hr>
  <center>
  <h1>ACTUALIZAR DATOS DE LOS PROPIETARIOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CEDULA DEL PROPIETARIO DEL CAN </label>
    <input class="form-control" value="<?php echo $propietario->cedula_pro; ?>" required="required"   type="number" name="cedula_pro" id="cedula_pro" placeholder="Ingrese su número de cédula">
    <br>
    <label for=""> NOMBRE DEL PROPIETARIO </label>
    <input class="form-control" value="<?php echo $propietario->nombre_pro; ?>" required="required" onkeypress="return validar(event)"  type="text" name="nombre_pro" id="nombre_pro" placeholder="Ingrese el nombre del dueño de la mascota">
     <br>
    <label for=""> APELLIDO  </label>
    <input class="form-control" value="<?php echo $propietario->apellido_pro; ?>" required="required" onkeypress="return validar(event)" type="text" name="apellido_pro" id="apellido_pro" placeholder="Ingrese el apellido del propietario">
    <br>
    <label for="">CIUDAD </label>
    <input class="form-control" value="<?php echo $propietario->ciudad_pro; ?>" required="required" onkeypress="return validar(event)" type="text" name="ciudad_pro" id="ciudad_pro" placeholder="Ingrese su ciudad: ejem, Quito">
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" value="<?php echo $propietario->direccion_pro; ?>" required="required"  type="text" name="direccion_pro" id="direccion_pro" placeholder="Ingrese su dirección domiciliaria">
    <br>
    <label for="">EMAIL</label>
    <input class="form-control" value="<?php echo $propietario->email_pro; ?>" required="required"  type="text" name="email_pro" id="email_pro" placeholder="Ingrese su dirección de correo electrónico">
    <br>
    <label for="">TELÉFONO </label>
    <input class="form-control" value="<?php echo $propietario->telefono_pro; ?>" required="required"  type="number" name="telefono_pro" id="telefono_pro" placeholder="ejemplo: 0958748525 ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/propietarios/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</form>
</div>
</div>
</div>
