<br>
<center>
  <hr>
  <h2>PROPIETARIO</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/propietarios/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar nuevo propietario  </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover " id="tbl-propietarios">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CÉDULA</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">CIUDAD</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">TELÉFONO</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()  as  $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->cedula_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->apellido_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->ciudad_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->direccion_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->email_pro;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->telefono_pro;?>
        </td>

        <td class="text-center">
          <a href="<?php echo site_url(); ?>/propietarios/editar/<?php echo $filaTemporal->id_pro;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_pro; ?>');" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron propietarios</h3>

  </div>

<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_pro){
        iziToast.question({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Esta seguro de eliminar los datos del propietario?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=
                    "<?php echo site_url(); ?>/propietarios/procesarEliminacion/"+id_pro;

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
  }
</script>

<script type="text/javascript">
$("#tbl-propietarios").DataTable({
  dom: 'lBfrtip',
  buttons: [
    'coppy','csv','excel','pdf','print'
  ]
});

</script>
