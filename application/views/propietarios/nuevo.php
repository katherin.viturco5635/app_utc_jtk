<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/propietarios/guardarPropietario"  method="post" id="frm_nuevo_propietario">
  <hr>
  <center>
  <h1>DATOS DE LOS PROPIETARIOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CEDULA DEL PROPIETARIO DEL CAN </label>
    <input class="form-control" required="required"   type="number" name="cedula_pro" id="cedula_pro" placeholder="Ingrese su número de cédula">
    <br>
    <label for=""> NOMBRE DEL PROPIETARIO </label>
    <input class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="nombre_pro" id="nombre_pro" placeholder="Ingrese el nombre del dueño de la mascota">
     <br>
    <label for=""> APELLIDO  </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="apellido_pro" id="apellido_pro" placeholder="Ingrese el apellido del propietario">
    <br>
    <label for="">CIUDAD </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="ciudad_pro" id="ciudad_pro" placeholder="Ingrese su ciudad: ejem, Quito">
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" required="required"  type="text" name="direccion_pro" id="direccion_pro" placeholder="Ingrese su dirección domiciliaria">
    <br>
    <label for="">EMAIL</label>
    <input class="form-control" required="required"  type="text" name="email_pro" id="email_pro" placeholder="Ingrese su dirección de correo electrónico">
    <br>
    <label for="">TELÉFONO </label>
    <input class="form-control" required="required"  type="number" name="telefono_pro" id="telefono_pro" placeholder="ejemplo: 0958748525 ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/propietarios/index"   class="btn btn-warning"><i class="fa fa-times"></i>  CANCELAR
    </a>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_propietario").validate({
      rules:{
        cedula_pro:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_pro:{
          letras:true,
          required:true
        },
        apellido_pro:{
          letras:true,
          required:true
        },
        ciudad_pro:{
          letras:true,
          required:true
        },
        direccion_pro:{
          required:true
        },
        email_pro:{
          required:true
        },
        telefono_pro:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
      },

      messages:{
        cedula_pro:{
          required:"Por favor ingrese el número de cédula.",
          minlength:"La cédula debe tener mínimo 10 digitos.",
          maxlength:"La cédula debe tener máximo 10 digitos.",
          digits: "La cédula solo acepta números."
        },
        nombre_pro:{
          letras:"El nombre solo debe tener letras.",
          required:"Por favor ingrese el nombre."
        },
        apellido_pro:{
          letras:"El apellido solo debe tener letras.",
          required:"Por favor ingrese el apellido."
        },
        ciudad_pro:{
          letras:"El nombre de la ciudad solo debe tener letras.",
          required:"Por favor ingrese la ciudad."
        },
        direccion_pro:{
          required:"Por favor ingrese la dirección."
        },
        email_pro:{
          required:"Por favor ingrese el correo electrónico."
        },
        telefono_pro:{
          required:"Por favor ingrese el número de teléfono celular.",
          minlength:"El número de celular debe tener mínimo 10 digitos.",
          maxlength:"El número de celular debe tener máximo 10 digitos.",
          digits:"El número de celular solo acepta números."
        }
        }
    });
</script>
