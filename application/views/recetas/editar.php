<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/recetas/procesarActualizacion"   method="post">
    <input type="hidden" name="id_rec" id="id_rec" value="<?php echo $receta->id_rec; ?>">
  <hr>
  <center>
  <h1>ACTUALIZAR REGISTRO DE RECETAS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> PROPIETARIO DEL CAN </label>
    <input class="form-control" value="<?php echo $receta->propietario_rec; ?>" required="required"  onkeypress="return validar(event)" type="text" name="propietario_rec" id="propietario_rec" placeholder="Ingrese  en nombre del nueño">
    <br>
    <label for=""> NOMBRE DEL PACIENTE </label>
    <input class="form-control" value="<?php echo $receta->paciente_rec; ?>" required="required" onkeypress="return validar(event)"  type="text" name="paciente_rec" id="paciente_rec" placeholder="Ingrese el nombre del paciente: EJM. Zeus">
     <br>
     <label for="">SEXO: </label>
     <select style="color:#000000" class="form-control" required="requiered" name="sexo_rec" id="sexo_rec">
         <option value="">--Seleccione--</option>
         <option value="Macho">Macho</option>
         <option value="Hembra">Hembra</option>
     </select>
    <br>
    <label for="">RAZA </label>
    <input class="form-control" value="<?php echo $receta->raza_rec; ?>" required="required" onkeypress="return validar(event)" type="text" name="raza_rec" id="raza_rec" placeholder="Indique la raza de su mascota">
    <br>
    <label for="">DIRECCION</label>
    <input class="form-control" value="<?php echo $receta->direccion_rec; ?>" required="required"  type="text" name="direccion_rec" id="direccion_rec" placeholder="Ingrese su dirección">
    <br>
    <label for="">EDAD</label>
    <input class="form-control" value="<?php echo $receta->edad_rec; ?>" required="required"  type="text" name="edad_rec" id="edad_rec" placeholder="Ingrese la edad de la mascota">
    <br>
    <label for="">FECHA</label>
    <input class="form-control" value="<?php echo $receta->fecha_rec; ?>" required="required"  type="date" name="fecha_rec" id="fecha_rec" placeholder="Seleccione ">
    <br>
    <label for="">DESCRIPCIÓN</label>
    <input class="form-control" value="<?php echo $receta->descripcion_rec; ?>" required="required"  type="text" name="descripcion_rec" id="descripcion_rec" placeholder="Registre los detalles del diagnostico realizado ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>  ACTUALIZAR   </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/recetas/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</form>
</div>
</div>
</div>

<script type="text/javascript">
  $("#sexo_rec").val("<?php echo $receta->sexo_rec; ?>");
</script>
