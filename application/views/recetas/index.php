<br>
<center>
  <hr>
  <h2>RECETA</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/recetas/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Añadir nueva receta
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover " id="tbl-recetas">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">PROPIETARIO</th>
        <th class="text-center">PACIENTE</th>
        <th class="text-center">SEXO</th>
        <th class="text-center">RAZA</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EDAD</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">DESCRIPCIÓN</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()      as  $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->propietario_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->paciente_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->sexo_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->raza_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->direccion_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->edad_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->fecha_rec;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->descripcion_rec;?>
        </td>

        <td class="text-center">
          <a href="<?php echo site_url(); ?>/recetas/editar/<?php echo $filaTemporal->id_rec;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_rec; ?>');" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron recetas</h3>

  </div>

<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_rec){
        iziToast.question({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Esta seguro de eliminar la receta?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=
                    "<?php echo site_url(); ?>/recetas/procesarEliminacion/"+id_rec;

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
  }
</script>

<script type="text/javascript">
$("#tbl-recetas").DataTable({
  dom: 'lBfrtip',
  buttons: [
    'coppy','csv','excel','pdf','print'
  ]
});
</script>
