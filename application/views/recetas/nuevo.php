<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/recetas/guardarReceta"
  method="post" id="frm_nuevo_receta">
  <hr>
  <center>
  <h1>REGISTRO DE RECETAS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> PROPIETARIO DEL CAN </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="propietario_rec" id="propietario_rec" placeholder="Ingrese  en nombre del nueño">
    <br>
    <label for=""> NOMBRE DEL PACIENTE </label>
    <input class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="paciente_rec" id="paciente_rec" placeholder="Ingrese el nombre del paciente: EJM. Zeus">
     <br>
     <label for="">SEXO: </label>
     <select class="form-control" name="sexo_rec" id="sexo_rec">
         <option value="">--Seleccione--</option>
         <option value="Macho">Macho</option>
         <option value="Hembra">Hembra</option>
     </select>
    <br>
    <label for="">RAZA </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="text" name="raza_rec" id="raza_rec" placeholder="Indique la raza de su mascota">
    <br>
    <label for="">DIRECCION</label>
    <input class="form-control" required="required"  type="text" name="direccion_rec" id="direccion_rec" placeholder="Ingrese su dirección">
    <br>
    <label for="">EDAD</label>
    <input class="form-control" required="required"  type="text" name="edad_rec" id="edad_rec" placeholder="Ingrese la edad de la mascota">
    <br>
    <label for="">FECHA</label>
    <input class="form-control" required="required"  type="date" name="fecha_rec" id="fecha_rec" placeholder="Seleccione ">
    <br>
    <label for="">DESCRIPCIÓN</label>
    <input class="form-control" required="required"  type="text" name="descripcion_rec" id="descripcion_rec" placeholder="Registre los detalles del diagnostico realizado ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <button href="<?php echo base_url(); ?>/receta/index"  type="submit" name="button" class="btn btn-primary"> <i class="fa fa-times "></i> CANCELAR</button>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_receta").validate({
      rules:{
        propietario_rec:{
          letras:true,
          required:true
        },
        paciente_rec:{
          letras:true,
          required:true
        },
        raza_rec:{
          required:true
        },
        sexo_rec:{
          required:true
        },
      direccion_rec:{
          required:true
        },
        edad_rec:{
          required:true
        },
        fecha_rec:{
            required:true
          },
        descripcion_rec:{
          letras:true,
          required:true
          },

      },

      messages:{
        propietario_rec:{
          letras:"El propietario solo debe tener letras.",
          required:"Por favor ingrese el propietario."
        },
        paciente_rec:{
          letras:true,
          required:"Por favor ingrese el nombre del paciente."
        },
        raza_rec:{
          required:"Por favor ingrese la raza de la mascota."
        },
        sexo_rec:{
          required:"Por favorseleccione el sexo."
        },
      direccion_rec:{
          required:"Por favor ingrese la dirección."
        },
        edad_rec:{
          required:"Por favor ingrese la edad de la mascota."
        },
        fecha_rec:{
            required:"Por favor seleccione la fecha."
          },
        descripcion_rec:{
          required:"Por favor ingrese la descripción.."
          }
      }
    });
</script>
