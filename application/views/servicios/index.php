<br>
<center>
  <hr>
  <h2>SERVICIOS</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/servicios/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i> Agregar una nueva servicio
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DEL SERVICIO</th>
        <th class="text-center">DESCRIPCION</th>
        <th class="text-center">TIPO DE SERVICIO</th>
        <th class="text-center">COSTO</th>
        <th class="text-center">FECHA DE INICIO</th>
        <th class="text-center">FECHA DE FINAL</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
            <?php echo $filaTemporal->id_ser; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_ser; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->descripcion_ser; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->tipo_ser; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->costo_ser; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->fecha1_ser; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->fecha2_ser; ?>
          </td>
        <td class="text-center">
          <a href="" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
          <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/servicios/procesarEliminacion/<?php echo $filaTemporal->id_ser;?>" class="btn btn-danger"> <i class="fa fa-trash"></i> </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron adopciones</h3>

  </div>

<?php endif; ?>
