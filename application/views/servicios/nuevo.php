<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/servicios/guardarservicio"
  method="post"
  >
  <hr>
  <center>
  <h1>DATOS DE LOS SERVICIOS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> NOMBRE DEL SERVICIO </label>
    <input class="form-control" required="required"   type="text" name="nombre_ser" id="nombre_ser" placeholder="Registre el nombre del servicio. ejm:radiografía.....">
    <br>
     <label for=""> DESCRIPCION </label>
     <input
      class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="descripcion_ser" id="descripcion_ser" placeholder="Ingrese una descripcion, ejm:lindo">
      <br>
     <label for="">TIPO DE SERVICIO: </label>
     <select class="form-control" name="tipo_ser" id="tipo_ser">
         <option value="">--Seleccione--</option>
         <option value="PROGRAMADO">PROGRAMADO</option>
         <option value="REVISION GENERAL">REVISION GENERAL</option>
         <option value="EMERGENCIA">EMERGENCIA</option>
         <option value="OTROS">OTROS</option>
     </select>
     <br>
    <label for=""> COSTO DEL SERVICIO </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="number" name="costo_ser" id="costo_ser" placeholder="Registre el costso: Ejm, 6 ">
    <br>
    <label for="">FECHA DE INICIO </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fecha1_ser" id="fecha1_ser" placeholder="ejemplo: Seleccione">
    <br>
    <label for="">FECHA DE FINALIZACION </label>
    <input class="form-control" required="required" onkeypress="return validar(event)" type="date" name="fecha2_ser" id="fecha2_ser" placeholder="ejemplo: Seleccione">
    <br>
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <button href="<?php echo base_url(); ?>/servicio/index"  type="submit" name="button" class="btn btn-primary"> <i class="fa fa-cancel "></i> CANCELAR</button>
</form>
</div>
</div>
</div>
