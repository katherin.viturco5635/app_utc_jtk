<br>
<center>
  <hr>
  <h2>SUCURSALES</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/sucursales/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i> Agregar una nueva servicio
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DE LA SUCURSAL</th>
        <th class="text-center">DIRECCION DE LA SUCURSAL</th>
        <th class="text-center">EMAL</th>
        <th class="text-center">TELEFONO</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()
      as $key => $filaTemporal): ?>

      <tr>
        <td class="text-center">
            <?php echo $filaTemporal->id_suc; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_suc; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->direccion_suc; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->email_suc; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->telefono_suc; ?>
          </td>

        <td class="text-center">
          <a href="" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
          <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/sucursales/procesarEliminacion/<?php echo $filaTemporal->id_suc;?>" class="btn btn-danger"> <i class="fa fa-trash"></i> </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron adopciones</h3>

  </div>

<?php endif; ?>
