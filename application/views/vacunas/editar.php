<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/vacunas/procesarActualizacion"  method="post">
    <input type="hidden" name="id_vac" id="id_vac" value="<?php echo $vacuna->id_vac; ?>">
  <hr>
  <center>
  <h1>ACTUALIZAR DATOS DE VACUNAS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CODIGO DE VACUNA </label>
    <input class="form-control" value="<?php echo $vacuna->codigo_vac; ?>" required="required"   type="text" name="codigo_vac" id="codigo_vac" placeholder="Ingrese el código de la vacuna">
    <br>
    <label for=""> NOMBRE DE VACUNA </label>
    <input class="form-control" value="<?php echo $vacuna->nombre_vac; ?>" required="required" onkeypress="return validar(event)"  type="text" name="nombre_vac" id="nombre_vac" placeholder="Indique el nombre de la vacuna">
    <br>
    <label for="">FECHA DE ELABORACION </label>
    <input class="form-control" value="<?php echo $vacuna->felaboracion_vac; ?>" required="required"  type="date" name="felaboracion_vac" id="felaboracion_vac" placeholder="Seleccione la fecha">
    <br>
    <label for=""> FECHA DE VENCIMIENTO </label>
    <input class="form-control" value="<?php echo $vacuna->fvencimiento_vac; ?>" required="required"  type="date" name="fvencimiento_vac" id="fvencimiento_vac" placeholder="Seleccione la fecha">
       <br>
     <label for="">TIPO DE VENTA: </label>
     <select style="color:#000000" class="form-control" name="tventas_vac" id="tventas_vac" required>
         <option value="">--Seleccione--</option>
         <option value="Venta_Libre">Venta_Libre</option>
         <option value="Prohibida_venta">Prohibida_venta</option>
     </select>
     <br>
    <label for=""> PRECIO </label>
    <input class="form-control" value="<?php echo $vacuna->precio_vac; ?>" required="required"  type="text" name="precio_vac" id="precio_vac" placeholder="Ingrese el valor de la vacuna">
    <br>
    <label for="">INDICACIONES </label>
    <input class="form-control" value="<?php echo $vacuna->indicaciones_vac; ?>" required="required"  type="text" name="indicaciones_vac" id="indicaciones_vac" placeholder="Ingrese las indicaciones de la vacuna">
    <br>
    <label for="">CANTIDAD </label>
    <input class="form-control" value="<?php echo $vacuna->cantidad_vac; ?>" required="required"  type="number" name="cantidad_vac" id="cantidad_vac" placeholder="Ingrese el número de vacunas ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i> ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/vacunas/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</form>
</div>
</div>
</div>

<script type="text/javascript">
  $("#tventas_vac").val("<?php echo $vacuna->tventas_vac; ?>");
</script>
