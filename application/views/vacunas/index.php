<br>
<center>
  <hr>
  <h2>VACUNAS</h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/vacunas/nuevo" class="btn btn-primary">
      <i class="fa fa-plus-circle "></i>  Agregar nueva vacuna
    </a>
    <br>
    <br>
</center>

<?php if ($listado): ?>
  <table class="table table-bordered table-striped table-hover " id="tbl-vacunas">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CÓDIGO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">FECHA DE ELABORACIÓN</th>
        <th class="text-center">FECHA DE VENCIMIENTO</th>
        <th class="text-center">TIPO DE VENTA</th>
        <th class="text-center">PRECIO</th>
        <th class="text-center">INDICACIONES</th>
        <th class="text-center">CANTIDAD</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listado->result()as  $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->codigo_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->felaboracion_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->fvencimiento_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->tventas_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->precio_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->indicaciones_vac;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->cantidad_vac;?>
        </td>
        <td class="text-center">
          <a href="<?php echo site_url(); ?>/vacunas/editar/<?php echo $filaTemporal->id_vac;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_vac; ?>');" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron alimentos</h3>

  </div>

<?php endif; ?>

<script type="text/javascript">
<<<<<<< HEAD
    function confirmarEliminacion(id_vac){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar la vacuna de forma permante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/vacunas/procesarEliminacion/"+id_vac;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
$("#tbl-vacunas").DataTable({
  dom: 'lBfrtip',
  buttons: [
    'coppy','csv','excel','pdf','print'
  ]
});

</script>
=======
          function confirmarEliminacion(id_vac){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar ela vacuna de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/vacunas/procesarEliminacion/"+id_vac;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>

      <script type="text/javascript">
      $("#tbl-clientes").DataTable();
        </script>
>>>>>>> a238a485d9f96af6a4c160bfa8f949f4498d3b1d
