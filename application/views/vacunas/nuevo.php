<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/vacunas/guardarVacuna"  method="post" id="frm_nuevo_vacuna"  >
  <hr>
  <center>
  <h1>DATOS DE VACUNAS</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> CODIGO DE VACUNA </label>
    <input class="form-control" required="required"   type="text" name="codigo_vac" id="codigo_vac" placeholder="Ingrese el código de la vacuna">
    <br>
    <label for=""> NOMBRE DE VACUNA </label>
    <input class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="nombre_vac" id="nombre_vac" placeholder="Indique el nombre de la vacuna">
    <br>
    <label for="">FECHA DE ELABORACION </label>
    <input class="form-control" required="required"  type="date" name="felaboracion_vac" id="felaboracion_vac" placeholder="Seleccione la fecha">
    <br>
    <label for=""> FECHA DE VENCIMIENTO </label>
    <input class="form-control" required="required"  type="date" name="fvencimiento_vac" id="fvencimiento_vac" placeholder="Seleccione la fecha">
       <br>
     <label for="">TIPO DE VENTA: </label>
     <select class="form-control" name="tventas_vac" id="tventas_vac" required>
         <option value="">--Seleccione--</option>
         <option value="Venta_Libre">Venta_Libre</option>
         <option value="Prohibida_venta">Prohibida_venta</option>
     </select>
     <br>
    <label for=""> PRECIO </label>
    <input class="form-control" required="required"  type="number" name="precio_vac" id="precio_vac" placeholder="Ingrese el valor de la vacuna">
    <br>
    <label for="">INDICACIONES </label>
    <input class="form-control" required="required"  type="text" name="indicaciones_vac" id="indicaciones_vac" placeholder="Ingrese las indicaciones de la vacuna">
    <br>
    <label for="">CANTIDAD </label>
    <input class="form-control" required="required"  type="number" name="cantidad_vac" id="cantidad_vac" placeholder="Ingrese el número de vacunas ">
    <br><br>
    <button type="submit" name="button" class="btn btn-info"> <i class="fa fa-save "></i> GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/vacunas/index"   class="btn btn-warning"><i class="fa fa-times"></i>  CANCELAR
    </a>
  </form>
</div>
</div>
</div>

<!-- AGREGAMOS TAG PARA LLAMAR AL FORMULARIO -->
<script type="text/javascript">
    $("#frm_nuevo_vacuna").validate({
      rules:{
        codigo_vac:{
          required:true
        },
        nombre_vac:{
          letras:true,
          required:true
        },
        felaboracion_vac:{
          required:true
        },
        fvencimiento_vac:{
          required:true
        },
        tventas_vac:{
          required:true
        },
        precio_vac:{
          required:true,
          minlength:1,
          maxlength:3,
          digits:true
        },
        indicaciones_vac:{
          letras:true,
          required:true
        },
        cantidad_vac:{
          required:true,
          minlength:1,
          maxlength:5,
          digits:true
        },
      },

      messages:{
        codigo_vac:{
          required:"Por favor ingrese el código de la vacuna."
        },
        nombre_vac:{
          letras:"El nombre solo debe tener letras.",
          required:"Por favor ingrese el nombre."
        },
        felaboracion_vac:{
          required:"Por favor seleccione la fecha de elaboración."
        },
        fvencimiento_vac:{
          required:"Por favor seleccione la fecha de vencimiento."
        },
        tventas_vac:{
          required:"Por favor seleccione el tipo de venta."
        },
        precio_vac:{
          required:"Por favor ingrese el precio.",
          minlength:"El precio debe tener mínimo 1 digitos.",
          maxlength:"El precio debe tener máximo 3 digitos.",
          digits:"El precio solo acepta números."
        },
        indicaciones_vac:{
          letras:"Las indicaciones solo debe tener letras.",
          required:"Por favor ingrese las indicaciones."
        },
        cantidad_vac:{
          required:"Por favor ingrese la cantidad de vacunas.",
          minlength:"La cantidad debe tener mínimo 1 digitos.",
          maxlength:"La cantidad debe tener máximo 5 digitos.",
          digits:"La cantidad solo acepta números."
        }
      }
    });
</script>
