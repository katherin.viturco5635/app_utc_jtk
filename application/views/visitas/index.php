<br>
<center>
  <hr>
  <h2>VISITAS DOMICILIARIA REGISTRADAS </h2>

</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/visitas/nuevo" class="btn btn-primary">
       <i class="fa fa-plus-circle "></i>  Agregar Nuevo
    </a>
    <br>
    <br>
</center>

<?php if ($listadoVisitas): ?>
  <table class="table" id="tbl-visitas">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRES PACIENTE</th>
        <th class="text-center">NOMBRES DUEÑO</th>
        <th class="text-center">TELÉFONO</th>
        <th class="text-center">HISTORIA CLÍNICA</th>
        <th class="text-center">DIRECCIÓN</th>
        <th class="text-center">SÍNTOMAS</th>
        <th class="text-center">NIVEL DE DOLOR</th>
        <th class="text-center">OPCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoVisitas->result() as $filaTemporal): ?>
      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombrep_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombred_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->telefono_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->historia_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->direccion_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->sintomas_vis;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->dolor_vis;?>
        </td>

        <td class="text-center">
          <a href="<?php echo site_url(); ?>/visitas/editar/<?php echo $filaTemporal->id_vis;?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
          <a href="javascript:void(0)"
              onclick="confirmarEliminacion ('<?php echo $filaTemporal->id_vis; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>
  </table>

  <script type="text/javascript">

  $('#tbl-visita').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  } );

  </script>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron CITAS por el momento</h3>

  </div>
<?php endif; ?>


<script type="text/javascript">

$('#tbl-visitas').DataTable( {
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ]
} );

</script>

<script type="text/javascript">
          function confirmarEliminacion(id_vis){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro de eliminar el registro de VISITAS de forma pernante?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            "<?php echo site_url(); ?>/visitas/procesarEliminacion/"+id_vis;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>
