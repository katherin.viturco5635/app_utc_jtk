<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/visitas/guardarVisita"
  method="post" id="frm_nuevo_visita"
  >
  <hr>
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <center>
  <h1>REGISTRE SU VISITA DOMICILIARIA:</h1>
  <hr>
  </center>

    <br>
    <br>

    <label for=""> NOMBRES DEL PACIENTE: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)" type="text" name="nombrep_vis" id="nombrep_vis" placeholder="ejemplo: Angel Amores">
    <br>
    <label for=""> NOMBRE DEL DUEÑO: </label>
    <input
     class="form-control" required="required" onkeypress="return validar(event)"  type="text" name="nombred_vis" id="nombred_vis" placeholder="ejemplo: Monta Almachi">
     <br>
     <label for=""> TELÉFONO: </label>
     <input class="form-control" required="required"   type="number" name="telefono_vis" id="telefono_vis" placeholder="Ingrese su numero de teléfono">
     <br>
     <label for="">HISTORIA CLÍNICA: </label>
     <input class="form-control" required="required" type="number" name="historia_vis" id="historia_vis" placeholder="Ingrese el número de historia clínica">

     <br>
    <label for=""> DIRECCIÓN: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)"   type="text" name="direccion_vis" id="direccion_vis" placeholder="Ingrese la dirección detallada de su ubicación actual">
    <br>
    <label for=""> SINTOMAS: </label>
    <input class="form-control" required="required"  onkeypress="return validar(event)"   type="tex" name="sintomas_vis" id="sintomas_vis" placeholder="INgrese lo sintomas que presenta el paciente">
    <br>
    <label for="">NIVEL DE DOLOR:</label>
    <select class="form-control" name="dolor_vis" id="dolor_vis">
        <option value="">--Seleccione--</option>
        <option value="Alta">Alta</option>
        <option value="Media">Media</option>
        <option value="Baja">Baja</option>
    </select>
    <br><br>
    <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save "></i>
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/visitas/index"
      class="btn btn-warning"> <i class="fa fa-times "></i> CANCELAR
    </a>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_visita").validate({
      rules:{
        nombrep_vis:{
          letras:true,
          required:true
        },
        nombred_vis:{
          letras:true,
          required:true
        },
        telefono_vis:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        historia_vis:{
          required:true,
          minlength:1,
          maxlength:5,
          digits:true
        },
        direccion_vis:{
          letras:true,
          required:true
        },
        sintomas_vis:{
          letras:true,
          required:true
        },
        dolor_vis:{
          required:true
        }
      },
      messages:{
        nombresp_vis:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el nombre del paciente solo con letras"
        },
        nombresd_vis:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el nombre del dueño solo con letras"
        },
        telefono_vis:{
          required:"Por favor ingrese el teléfono en números",
          minlength:"El número de edad debe tener mínimo 10 digitos",
          maxlength:"El número de edad debe tener máximo 10 digitos",
          digits:"El número de teléfono solo acepta números"
        },
        historia_vis:{
          required:"Por favor ingrese la historia clínica en números",
          minlength:"El número de historia debe tener mínimo 1 digitos",
          maxlength:"El número de historia debe tener máximo 5 digitos",
          digits:"El número de historia solo acepta números"
        },
        direccion_vis:{
          letras:"Ingrese solamente letras",
          required:"Ingrese la dirección solo con letras"
        },
        sintomas_vis:{
          letras:"Ingrese solamente letras",
          required:"Ingrese laos síntomas solo con letras"
        },
        dolor_vis:{
          required:"Por favor seleccione el nivel de dolor"
        }
      }

  });
</script>
