
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="https://www.clinicaraza.com/hubfs/URGENCIAS%20VETERINARIAS.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://www.centroveterinariohuellas.com/uploads/XVj0IfpJ/Centro-Veterinario-Huellas-banner2.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://previews.123rf.com/images/visivasnc/visivasnc1709/visivasnc170900101/86947354-veterinario-con-estetoscopio-en-bolsillo-perro-y-gato-concepto-de-banner-cl%C3%ADnica-veterinaria-.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

<br>
<h4>  <b>MISIÓN</b> </h4>
<p>Garantizar la salud y el bienestar animal protegiendo, tanto de manera directa como indirecta, la salud pública.
	El control de las enfermedades animales y la información permiten que la población animal de nuestro entorno esté
	protegida y cuidada. Igualmente, la prevención de ciertas enfermedades zoonóticas es esencial en un entorno que
	comparten animales y personas.</p>
</div>
</div>
